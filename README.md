# Marvel Super Heroes RESTful API Project

Small application that allows users to fetch basic information on and add custom notes about the creators of the vast  [Marvel Super Heroes](https://developer.marvel.com/docs#!/public/) universe.

# Getting Started

These instructions will get you a copy of the project on your local machine and guide you through the process of building, configuring and deploying it locally or/and remotely.

# Prerequisites

To run this project on your local machine you should have the following tools installed (versions used may possibly be different, but these were the ones actually used):

```
Java 8
Maven 3.5
Git 2.8
Apache Tomcat 8.5.15
PostgreSQL 9.5.7
JUnit 4
```


I will not go into details about the installation and classpath setting of the tools mentioned above since it is pretty straight forward. In any case, if problems arise, there are several available tutorials and help online on how to do that. I will, however, detail every configuration that is not default and each process needed to successfully configure the application.

#**Project Component 1: Marvel Heroes REST API**


## Cloning the project 

The first step is cloning the project into your local machine. To do so, open the console and run:

```
$ git clone https://bitbucket.org/pmrg/marvelheroesapi.git
```

## Building the project

Before building the project we need to add a file named 'keys.properties' to the resources folder of the project ```(../marvelheroesapi/src/main/resources/)``` with the following properties defined and filled with the personal keys obtained from Marvel to access their API:

```
#public key obtain from Marvel 
marvel.api.keys.public=
#private key obtain from Marvel 
marvel.api.keys.private=
```
Depending if you want to deploy the war locally or remotely, you will need to select the correct database connector. This can be done in the file *"config.properties"*:

```
#description: database connector to use
#rules: case insensitive, mandatory
#available: localPostgresql,  HerokuPostgresql
database.context=
```

After this, make sure you are in the root of the project you just cloned and run:

```
$ mvn clean package
```

This will generate the .war needed to deploy into the web server (Apache Tomcat) as I will explain ahead.


## SQL and Database creation


The data model of the application is quite simple. We have a table of ```Users``` (that must be registered in order to use the App) and also a table of ```Notes```, that will store the notes we create. A weak entity relates our Users with the Notes ```('user_notes')``` they create and own, normalizing the database and thus avoiding data redundancy to be spread throughout our database. To maintain data integrity, we have also a couple of ```triggers/procedures```, one that verifies if a Note is a duplicate and prevents the insertion and another one to delete the references of no longer existing Notes (*).  
I named my database ```notes```. This is important, because this name will be used in Tomcat configuration shown in the next steps, so they must match. The database definition and creation file can be found at ```(../marvelheroesapi/src/SQL/CreateTables.sql)```. 


(***NOTE:*** The way of checking if a user existed in database was done through a trigger at Note insertion time only, however I needed to changed that because in some cases we could avoid unnecessary background work by validating the user existence sooner. I realized that after removing the trigger I have not added this "checkExistence" for all the operations, so it may happen that you don't need to be registered in the database to access resources at the time of writing this note. This will be corrected in next versions of the application.)

## Deploying the project, Configuring and Running Apache Tomcat

Changes to some default configurations are needed in order for our application function as expected.  Before we can start the server, we will need to set the database resource information into Apache Tomcat config file. Also, in order to deploy our recently created .war to Tomcat automatically, some additional changes need to be done. While the former are mandatory, the latter are only optional, since one can just deploy the war using Tomcat manager directly or using other process. Nevertheless, I will explain how to do it automatically with Maven anyway. 

### Database Context

Setting the database resource context will create a connection pool to our database, with customisable parameters depending on one's needs. Using a pool of connections to access the database is very advised for improved scalability and performance and some databases wont even work if there is only one connection shared among several clients. 
Open ```(../apache-tomcat-X.x.xx/conf/context.xml``` and add the following piece of XML. This will start the pool with 5 connections and allows a maximum of 10, which is customisable.

```
<Context>
  <Resource name="jdbc/notes" auth="Container" type="javax.sql.DataSource"
    username="postgres" password="admin"
    url="jdbc:postgresql://localhost:5432/notes"
    driverClassName="org.postgresql.Driver"
    initialSize="5" maxWaitMillis="1000"
    maxTotal="10" maxIdle="5"
    validationQuery="select 1"
    poolPreparedStatements="true"/>
</Context>
```

 We will be using some of this properties in our Java code while looking up for our database connector. These are already set in a properties file ```(config.properties)``` in our project 'Resources' folder. If you desire to change the name of your database, you need to make sure to change the lookup parameter as well.


```
#description: local database datasource (name must match the defined in tomcat # configs)
#rules: case sensitive, mandatory
database.envCtx.lookup=jdbc/notes
```



### Database Driver

In order for Tomcat to be able to connect to the database, one needs yet to add the database driver ```.jars``` to the ```/lib``` folder of Tomcat. The drivers used in this project were:

```
postgresql-9.2-1004.jdbc4.jar
postgresql-42.1.1.jre7.jar
```

#### Automatic deploy to Tomcat using Maven

If you wish to use this feature, open ```../conf/tomcat-users.xml``` inside Tomcat folder and define a new user with this privileges:

```
 <tomcat-users>
       <role rolename="manager-gui"/>
       <role rolename="manager-script"/>
       <user username="admin" password="admin" roles="manager-gui,manager-script,admin" />
</tomcat-users>
```

One can obviously change the values defined for username and password, just make sure they match with those you will be setting next. Inside Maven installation folder ```(../apache-maven-3.x.x/conf/settings.xml)``` add:

```
<servers>
  <server>
    <id>TomcatServer</id>
     <username>admin</username>
     <password>admin</password>
  </server>
</servers>
```

Again, if changes to these values are made they will need to be reflected in the application project ```POM``` file ```(../marvelheroesapi/pom.xml)```, because the server name is referenced there:

```
<plugin>
    <groupId>org.apache.tomcat.maven</groupId>
    <artifactId>tomcat7-maven-plugin</artifactId>
    <version>2.2</version>
    <configuration>
        <url>http://localhost:8080/manager/text</url>
        <server>TomcatServer</server>
        <path>/${project.artifactId}</path>
        <warDirectory>${webappDirectory}</warDirectory>
        <warFile>${webappDirectory}.war</warFile>
    </configuration>
</plugin>
```

#### Running Tomcat 

**Before deploying** the application to the server, make sure Tomcat it is running. Go to ```(../apache-tomcat-X.x.xx/bin) ``` and run the script:

```
./startup.bat (windows) or ./startup.sh (linux)
```

To shutdown the server run the script 'shutdown' in the same folder.


#### Deploying to Tomcat 

**After starting** the web server you can run the following command and the .war will be deployed to the server:

```
$ mvn tomcat7:redeploy
```

You should see a message of success similar to that below: 

```
(...)
[INFO] --- maven-war-plugin:2.3:war (default-war) @ MarvelSuperHeroesAPI ---
[INFO] Packaging webapp
[INFO] Assembling webapp [MarvelSuperHeroesAPI] in [(...)/marvelheroesapi/target/MarvelSuperHeroesAPI]
[INFO] Processing war project
[INFO] Copying webapp resources [(...)/marvelheroesapi/src/main/webapp]
[INFO] Webapp assembled in [564 msecs]
[INFO] Building war: (...)/marvelheroesapi/target/MarvelSuperHeroesAPI.war
[INFO]
[INFO] <<< tomcat7-maven-plugin:2.2:redeploy (default-cli) < package @ MarvelSuperHeroesAPI <<<
[INFO]
[INFO]
[INFO] --- tomcat7-maven-plugin:2.2:redeploy (default-cli) @ MarvelSuperHeroesAPI ---
[INFO] Deploying war to http://localhost:8080/MarvelSuperHeroesAPI
Uploading: http://localhost:8080/manager/text/deploy?path=%2FMarvelSuperHeroesAPI&update=true
Uploaded: http://localhost:8080/manager/text/deploy?path=%2FMarvelSuperHeroesAPI&update=true (10507 KB at 9389.0 KB/sec)

[INFO] tomcatManager status code:200, ReasonPhrase:
[INFO] OK - Deployed application at context path [/MarvelSuperHeroesAPI]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.640 s
[INFO] Finished at: 2017-07-10T01:33:34+01:00
[INFO] Final Memory: 37M/276M
[INFO] ------------------------------------------------------------------------
```

You can check by accessing http://localhost:8080/MarvelSuperHeroesAPI/v1/verify if our application is online and running:

```
{
    "Service Status": "ONLINE"
}
```

## **Deploying the project to a cloud-based server (Heroku)**
In this section I will explain the steps necessary to deploy our application to a cloud-based server, in this case, Heroku. In order to do so, you will need an account on this platform. 

### Useful tips/commands 

#### ```--app``` flag

If for some reason heroku complains about the 'app name' not being defined, use the flag '- -app <appName>' after the command. This happened to me while trying to execute commands in a different folder other than the one where the app was created, so if you are always executing from the same folder you won't need it.

#### ```echo``` command

This will output the result of the commands we are executing in the server. It is useful to see if certain variables are defined or even to see the result of the execution of a command, like you will see in the next sections.

#### ```logs --tail``` command

This command will be useful for checking the logs of the operations being executed in the remote server. To use it, just run: 

``` $ heroku logs --tail```


### Creating and deploying the application
To create and deploy the application to heroku, you will need to run the following commands:
```
$ heroku create (creates the application)
```
```
-(Optional: rename the app: see how to do this in the next section)
```
```
$ git push heroku master (this will push the changes made to Heroku. We can do this only at the end of the process, but it is recommended to do so now, so we don't forget)
```
```
-Creating the database (see respective section)
```
```
-Deploying the war (see respective section)
```
```
-Opening it! (see respective section)
```

#### Renaming an App
To rename an app, just run:
```
$ heroku apps:rename <newAppName>
```
You will see this output:
```
Renaming <old-app-name> to <newAppName>... done
https://<newAppName>.herokuapp.com/ | https://git.heroku.com/<newAppName>.git
Git remote heroku updated
 !    Don't forget to update git remotes for all other local checkouts of the
 !    app.

```

### Setting up an SQL Database (postgresql) in Heroku

#### **1) Creating database add-on in the server**

```
$ heroku addons:create heroku-postgresql
```
```
Creating heroku-postgresql on <appName>... free
Database has been created and is available
 ! This database is empty. If upgrading, you can transfer
 ! data from another database with pg:copy
Created postgresql-defined-89897 as DATABASE_URL
Use heroku addons:docs heroku-postgresql to view documentation
```

Check if database add-on was set. If if the variable 'JDBC_DATABASE_URL' is defined on the server, it was created successfully.
In order to see this echo, I needed to push the the master to the server first (``` $ git push heroku master ```). However, the command is executed anyway even if you only push at the end of all steps (I tested it). You won't be able to see the echo though.


```
$ heroku run echo \$JDBC_DATABASE_URL
```
And the output:
```
Running echo $JDBC_DATABASE_URL on <appName>... starting, run.4833 (Free)
Running echo $JDBC_DATABASE_URL on <appName>... connecting, run.4833 (Free)
Running echo $JDBC_DATABASE_URL on <appName>... up, run.4833 (Free)
jdbc:postgresql://<$IP>.compute-1.amazonaws.com:<$PORT>/de37houaj6e4hd?user=zlzaxynwwpllzn&password=0a9966dc60849d3bb96889375ff1a5bf7ab61b5bf80b641bc08dee583a23f459&sslmode=require
```



#### **2) Creating the database**  

To create the tables on the server, you run ```$ cat <pathToFile> | heroku pg:psql```.  This will output the content of the document to heroku and create the tables:

```
$ cat src/SQL/CreateTables.sql | heroku pg:psql
```
The output:
```
--> Connecting to postgresql-objective-38026
DROP SCHEMA
CREATE SCHEMA
CREATE SEQUENCE
CREATE SEQUENCE
CREATE SEQUENCE
CREATE TABLE
CREATE TABLE
CREATE TABLE
CREATE FUNCTION
CREATE TRIGGER
CREATE FUNCTION
CREATE TRIGGER
INSERT 0 1
INSERT 0 1
```

####**3) Checking if the database was correctly created**

```
$ echo "select * from users" | heroku pg:psql
```

```
--> Connecting to postgresql-objective-38026
 id |  name
----+--------
  1 | Pedro
  2 | Miguel
(2 rows)
```


### Deploying the war 
The following command will deploy our war to heroku. It may take some time to finish, so just wait until it is done.
```
$ mvn clean heroku:deploy-war
```

#### Opening the App

You can open an app from the browser, or run the following command, that will do it for you: 

```
$ heroku open
```

## Propagating  changes in the local code to Heroku

- add changes to stage: ``` git add . ```
- commit to git: ```git commit -m "commit msg" ```
- push changes to master: ```git push heroku master ```


## Changes to ***pom.xml***

We need to add/change some plugins/dependencies in order to deploy our war to heroku:

Add:
```
<plugin>
    <groupId>com.heroku.sdk</groupId>
    <artifactId>heroku-maven-plugin</artifactId>
    <version>1.1.3</version>
</plugin>

```

```
<dependency>
    <groupId>commons-dbcp</groupId>
    <artifactId>commons-dbcp</artifactId>
    <version>1.2.2</version>
</dependency>
```

Change *maven-dependency-plugin*, adding another execution needed by the remote server.
(**Note**: distinguish both executions by providing an ```<id></id>``` property for each, otherwise Maven will complain about the POM being invalid):
```		
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-dependency-plugin</artifactId>
    <version>3.0.1</version>
    <executions>
        <execution>
            <id>remote</id>
            <phase>package</phase>
            <goals>
                <goal>copy</goal>
            </goals>
            <configuration>
                <artifactItems>
                    <artifactItem>
                        <groupId>com.github.jsimone</groupId>
                        <artifactId>webapp-runner</artifactId>
                        <version>8.5.11.3</version>
                        <destFileName>webapp-runner.jar</destFileName>
                    </artifactItem>
                </artifactItems>
            </configuration>
        </execution>
        <execution>
            <id>local</id>
            <phase>validate</phase>
            <goals>
                <goal>copy</goal>
            </goals>
            <configuration>
                <outputDirectory>${endorsed.dir}</outputDirectory>
                <silent>true</silent>
                <artifactItems>
                    <artifactItem>
                        <groupId>javax</groupId>
                        <artifactId>javaee-endorsed-api</artifactId>
                        <version>7.0</version>
                        <type>jar</type>
                    </artifactItem>
                </artifactItems>
            </configuration>
        </execution>
    </executions>
</plugin>
```



# Application Endpoints, parameters and error messages 

This section describes the available endpoints of the API, how to call them and the responses you should expect to get. As stated before, the application will serve to fetch information on [Marvel Super Heroes](https://developer.marvel.com/docs#!/public/) creators, so half of my job for this section is already done, since the list of default parameters supported is described there (nevertheless our App will provide even more functionality, as described in the next sections). 


The application proposed here serves then as a wrapper and decorator of Marvel's API, since it supports every ```Parameter``` described in [GET /v1/public/creators ](https://developer.marvel.com/docs#!/public/getCreatorCollection_get_12) and adds a set of custom ones, used to create and manage our personal ```*Notes*``` on the creators and also to filter the results returned by Marvel's API even further. 

All responses and requests that need to have some type of payload, are defined as JSON.

##  **Creators Endpoints** 

Since this application is all about the amazing Marvel creators, let's start by describing this one first. 

###  GET /v1/{*username*}/creators/list

A call here will have a Response similar to the one depicted below, depending of course on the query parameters used in the call:

```
{
    "response info": {
        "offset": 0,
        "creators": [
            {
                "stories": 0,
                "series": 0,
                "comics": 0,
                "fullName": "",
                "modified": "-0001-11-30T00:00:00-0500",
                "id": 7968,
                "customNotes": {
                    "notes": [],
                    "notesCount": 0
                },
                "events": 0
            }
        ],
        "count": 1,
        "limit": 1,
        "totalCount": 6115
    },
    "status": "Success"
}
```
where ```"response info"``` will have the information on creators filtered and sorted by the query parameters sent and ```"status"``` will have the result of the operation, Success or Failed. 
Additionally, each Response has a ```Status Code``` defined in its header that state the status of the HTTP call itself and also the following ones: 
```
Content-Length → xxx
Content-Type →application/json
Date →Tue, 11 Jul 2017 20:11:58 GMT 
```

As we can see by the results, the above API call had a limit=1 query parameter, so it only returned 1 result of 6115 total existing results. Additionally there is a "customNotes" property associated with each element returned in the "creators" array. These are the custom notes added to that creator. In this case, since we have not added any, the array is empty.



####Filters and Sorters 

In addition to the default list of ```filters``` and ```orderBy``` options defined Marvel API, we have our custom list as well. One must pay attention that our custom filters are always applied after the default ones and are applied by the order they appear in the query. This means that only the subset of creators returned by the Marvel API call will be further filtered/sorted.

-------------------------------------
#####Default filters list
-------------------------------------

- **firstName**: Filter by creator first name (e.g. Brian).
- **middleName**: Filter by creator middle name (e.g. Michael).
- **lastName**: Filter by creator last name (e.g. Bendis).
- **suffix**: Filter by suffix or honorific (e.g. Jr., Sr.).
- **nameStartsWith**: Filter by creator names that match critera (e.g. B, St L).
- **firstNameStartsWith**: Filter by creator first names that match critera (e.g. B, St L).	
- **middleNameStartsWith**: Filter by creator middle names that match critera (e.g. Mi).	
- **lastNameStartsWith**: Filter by creator last names that match critera (e.g. Ben).	
- **modifiedSince**: Return only creators which have been modified since the specified date.
- **comics**: Return only creators who worked on in the specified comics (accepts a comma-separated list of ids).
- **series**: Return only creators who worked on the specified series (accepts a comma-separated list of ids).
- **events**: Return only creators who worked on comics that took place in the specified events (accepts a comma-separated list of ids).
- **stories**: Return only creators who worked on the specified stories (accepts a comma-separated list of ids).
- **limit**: Limit the result set to the specified number of resources.
- **offset**: Skip the specified number of resources in the result set.


----------

##### Custom filters list

----------


This list of custom filters include: 

- ***id:*** Filter by creator id.  
- ***fullNameEquals:*** Filter by creator fullName.
- ***fullNameContains:*** Filter by creator fullName that match criteria.
- ***notesNr:*** Filter by creators with given number of notes.
- ***comicsNr:*** Filter by creators with given number of comics.
- ***storiesNr:*** Filter by creators with given number of stories.
- ***seriesNr:*** Filter by creators with given number of series.
- ***eventsNr:*** Filter by creators with given number of events.

----------
##### Default sorting options
----------

**orderBy:** Order the result set by a field or fields. Add a "-" to the value sort in descending order. Multiple values are given priority in the order in which they are passed:

- **firstName**
- **lastName**
- **middleName**
- **suffix**
- **modified**	



-------------------------------------
##### Custom sorters list
----------

The list of custom sorters includes the following parameters. Add “asc or desc” to the value to define the sorting order. Multiple values are given priority in the order in which they are passed:

- ***sortById***
- ***sortByFullName***
- ***sortByModified***
- ***sortByNotesNr***
- ***sortByComicsNr***
- ***sortByStoriesNr***
- ***sortBySeriesNr***
- ***sortByEventsNr***


Usage example:
```
GET (...)/creators/list?sortByNotesNr=desc&sortByFullName=desc&lastName=Diaz
```

```
{
    "response info": {
        "offset": 0,
        "creators": [
            {
                "stories": 0,
                "series": 0,
                "comics": 0,
                "fullName": "Wellington Diaz",
                "modified": "2007-01-02T00:00:00-0500",
                "id": 9668,
                "customNotes": {
                    "notes": [],
                    "notesCount": 0
                },
                "events": 0
            },
            {
                "stories": 37,
                "series": 9,
                "comics": 24,
                "fullName": "Ruben Diaz",
                "modified": "2007-01-01T00:00:00-0500",
                "id": 4145,
                "customNotes": {
                    "notes": [],
                    "notesCount": 0
                },
                "events": 0
            },
            {
                "stories": 74,
                "series": 32,
                "comics": 65,
                "fullName": "Paco Diaz",
                "modified": "2015-06-16T11:37:43-0400",
                "id": 4150,
                "customNotes": {
                    "notes": [],
                    "notesCount": 0
                },
                "events": 4
            },
            {
                "stories": 0,
                "series": 0,
                "comics": 0,
                "fullName": "Jun Diaz",
                "modified": "2007-01-02T00:00:00-0500",
                "id": 8858,
                "customNotes": {
                    "notes": [],
                    "notesCount": 0
                },
                "events": 0
            },
            {
                "stories": 0,
                "series": 0,
                "comics": 0,
                "fullName": "Francisco Paco Diaz",
                "modified": "2007-01-02T00:00:00-0500",
                "id": 8500,
                "customNotes": {
                    "notes": [],
                    "notesCount": 0
                },
                "events": 0
            }
        ],
        "count": 5,
        "limit": 20,
        "totalCount": 5
    },
    "status": "Success"
}
```

Our application as a **custom default limit** of **20** results, but we can return a **maximum** of **100**, which corresponds to the maximum allowed by Marvel API. 
In the above case, the **total of results** returned was **below** that limit, **5**. The **count** was also **5**, meaning that our custom filters were not discriminatory, i.e, all the results returned by Marvel also matched our filters. However, if we have a filter by **seriesNr=9**, only one result will match:

```
GET (...)/creators/list?sortByNotesNr=desc&sortByFullName=desc&lastName=Diaz&seriesNr=9
```

```
{
    "response info": {
        "offset": 0,
        "creators": [
            {
                "stories": 37,
                "series": 9,
                "comics": 24,
                "fullName": "Ruben Diaz",
                "modified": "2007-01-01T00:00:00-0500",
                "id": 4145,
                "customNotes": {
                    "notes": [],
                    "notesCount": 0
                },
                "events": 0
            }
        ],
        "customFilteredResults": 20,
        "count": 1,
        "limit": 20,
        "totalCount": 5
    },
    "status": "Success"
}
```

### GET /v1/{username}/creators/compare/{creatorId1}/{creatorId2}

A successful call to this endpoint will return a comparison summary between the two creators identified by the respective provided ids. An error response is given if an invalid or nonexistent id is given. 

The summary for each creator includes:

- *id*, *first name*, *middle name*, *last name* and *modified date*;
- the *custom notes* and *number* of notes ;
- the *# comics*, *# series*, *# stories*, *# events* of each creator;
- the *# comics*, *# series*, *# stories*, *# events* in common between each;
- the *# of times* *they were* *collaborators*, i.e., the number of comics in which the specified creators worked together.

Example:

```(...)/creators/compare/6606/420```

Success: 200 OK
```
{
    "response info": {
        "creators": [
            {
                "firstName": "A.R.K.",
                "lastName": "",
                "stories": 1,
                "series": 1,
                "comics": 1,
                "modified": "2007-01-02T00:00:00-0500",
                "middleName": "",
                "id": 6606,
                "customNotes": {
                    "notes": [],
                    "notesCount": 0
                },
                "events": 0
            },
            {
                "firstName": "Sandu",
                "lastName": "Florea",
                "stories": 103,
                "series": 49,
                "comics": 101,
                "modified": "2013-02-19T11:40:27-0500",
                "middleName": "",
                "id": 420,
                "customNotes": {
                    "notes": [],
                    "notesCount": 0
                },
                "events": 5
            }
        ],
        "commonResourceCount": {
            "commonComicsCount": 102,
            "commonSeriesCount": 50,
            "commonEventsCount": 5,
            "commonStoriesCount": 100,
            "collaborators": 0
        }
    },
    "status": "Success"
}
```
Failed: 400 BAD REQUEST:
```
{
    "response info": {
        "status message": "Sorry, '42043543543' is an invalid Id!",
        "status": "Failed"
    }
}
```

Success: 200 OK from our side, 404 Not Found from Marvel API call: 
```
{
    "response info": {
        "status message": {
            "code": 404,
            "status": "We couldn't find that creator"
        },
        "status": "Success"
    }
}
```


## Notes Endpoint 


### GET (...)/v1/{username}/notes

A successful call (like the one displayed below) will return a list of notes (array "response data") associated with the *username* who created them and will contain the information of the creator to whom it was created for. 
Additionally we have ***hrefs*** to **next**, **previous**, **first** and **last** pages of the results, if they exist. When a *limit* and *offset* is not provided, default values are used and shown in the *hrefs* returned. 
We can filter notes by all their fields, which will be listed ahead.

Example:
```
{
    "response info": {
        "next": "http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes?limit=2&offset=5",
        "previous": "http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes?limit=2&offset=1",
        "count": 2,
        "totalCount": 9
    },
    "response data": [
        {
            "modificationDate": "2017-07-12 01:11:13.0",
            "creatorName": "A.R.K.",
            "creatorId": 6606,
            "description": "Another note about this creator!",
            "href": "http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes/ebec57a7afbf29f192df0949c1537aaa",
            "creationDate": "2017-07-12 01:11:13.0",
            "hash": "ebec57a7afbf29f192df0949c1537aaa"
        },
        {
            "modificationDate": "2017-07-12 01:11:20.0",
            "creatorName": "A.R.K.",
            "creatorId": 6606,
            "description": "Damn, so many notes!",
            "href": "http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes/5674ad47c9957bc268aafdebba5fecb7",
            "creationDate": "2017-07-12 01:11:20.0",
            "hash": "5674ad47c9957bc268aafdebba5fecb7"
        }
    ]
}
```


----------

#### Filters

----------

- ***creatorId***: Filter notes whose creatorId matches the provided value.
- ***creatorNameContains***: Filter notes whose creatorName contains the provided value.
- ***creatorNameEquals***: Filter notes whose creatorName matches the provided value.
- ***creationDate***: Filter notes created in that date (E.g: 2017-07-12)
- ***modificationDate***: Filter notes last modified in that date (E.g: 2017-07-12)
- ***descriptionContains***: Filter notes whose description contains the provided value.
- ***descriptionEquals***: Filter notes whose description matches the provided value.
- **limit**: Limit the result set to the specified number of resources.
- **offset**: Skip the specified number of resources in the result set.


Example:

(...)/v1/Pedro/notes?descriptionEquals=Can%20you%20stop%20already?!&creatorNameContains=Paco&creatorId=4150
```
{
    "response info": {
        "next": "",
        "previous": "",
        "count": 1,
        "totalCount": 1
    },
    "response data": {
        "notes": [
            {
                "modificationDate": "2017-07-12 01:12:48.0",
                "description": "Can you stop already?!",
                "href": "http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes/86f85185ab011e0a295750b8452b64c4",
                "creationDate": "2017-07-12 01:12:48.0",
                "hash": "86f85185ab011e0a295750b8452b64c4"
            }
        ],
        "creatorId": 4150,
        "creatorName": "Paco Diaz"
    }
}
```


### GET (...)/v1/{username}/notes/{*hash*}
 
(***NOTE***: This is one of the cases where I should do the verification for the username registration and I am not yet. So it happens that we can receive a 200 OK success with count=0 indicating that there is no note with the provided hash, or it can be a user that is not registered in the database. Please check the username you are providing and be sure that it is registered in the database before I can update the version of the App) 

200 OK note not found:
```
{
    "response info": {
        "count": 0
    },
    "response data": []
}
```
200 OK note found  
```
{
    "response info": {
        "count": 1
    },
    "response data": [
        {
            "modificationDate": "2017-07-12 01:12:54.0",
            "creatorName": "Francisco Paco Diaz",
            "creatorId": 8500,
            "description": "A note about this creator!",
            "creationDate": "2017-07-12 01:12:54.0",
            "hash": "f8520e429ef778f5820f90538227a125"
        }
    ]
}
```


###POST (...)/v1/{*username*}/notes/add

To add a new note, a POST request should be send to the respective endpoint containig the **creatorId** and the **description**. The JSON schema that defines a valid POST can be found at ```/src/main/resources```. Some examples of returned responses are shown below:

```400 Bad Request:```
```
{
    "response info": {
        "status message": "Failed to insert Note: User does not exist",
        "status": "Failed"
    }
}
```

```
{
    "response info": {
        "status message": "Failed schema validation: [#/creatorId: expected type: Number, found: Boolean]",
        "status": "Failed"
    }
}
```


Note that a ```200 OK``` may not always mean that a note was added correctly. In the example below, a call to our endpoint was successful and passed all verification, but when we called ```Marvel's API``` for a creator with the given id, it was not found:

```200 OK```
```
{
    "response info": {
        "status message": {
            "code": 404,
            "status": "We couldn't find that creator"
        },
        "status": "Success"
    }
}
```


### DELETE (...)/v1/{*username*}/notes/delete 

To delete a note, we will need to provide a valid **hash**. JSON schema is available in ```/src/main/resources```.

### PUT (...)/v1/{*username*}/notes/update

To update a note, we send the **hash** associated with a note and the new **description**.

(**NOTE**: The same happens here, if we have an invalid name as username, we receive that the node was not found (because the sql query checks if there is a note with that username associated) so check the URI you are trying to access first)


A successful insertion, deletion and edition of a note will return a ```200 OK```. The "status message" will change depending on the operation performed.
```
{
    "response info": {
        "status message": "Note inserted.",
        "status": "Success"
    }
}
```


# **Project Component 2: Command Line Tool (MarvelCMD)**

The goal of the command line tool is to provide an interface to the API. It is very easy to use and it is self explanatory how to do so, since there is an 'help' command that describes each supported operation and its usage.

##Cloning the project

```
$ git clone https://pmrg@bitbucket.org/pmrg/marvelcmd.git
```

## Building the project



To run the tool, go to its root folder and run the command below. This will build our client .jar file.

```
$ mvn clean package
```

 Next, simply run:

```
$ mvn exec:java
```

This will start the tool and you will this in the console:

```
--------- Usage: ----------
Connecto to local server args list: ['local' 'ip' 'port' 'appName' 'appVersion' 'username']
Connecto to remote server args list: ['remote' 'subdomain/appName' 'domain' 'appVersion' 'username']
E.g. if using Maven:
>> $ mvn exec:java -Dexec.args="local localhost 8080 MarvelSuperHeroesAPI v1 Pedro"
>> $ mvn exec:java -Dexec.args="remote marvel-super-heroes-api herokuapp.com v1 Pedro"
----------Exiting----------
```
This means that the number of arguments provided was not correct. The correct usage is then displayed.
From here all is self explanatory and the tool will do requests to the predefined service URL's. 

Providing the correct number of arguments you will then see:

```
--------------
Accessing remote version of the application.
--------------
Subdomain: marvel-super-heroes-api
Domain: herokuapp.com
Version: v1
Username: Pedro
Service uri: http://marvel-super-heroes-api.herokuapp.com/v1
--------------
Welcome to 'Marvel Super Heroes' Command Line Tool!
Please enter your <command> or press <Enter> to get the list of available commands. Write <exit> to terminate session.

>>
```

## Using the tool to call the API

As an example, pressing ```<Enter>``` will result in:

```
Available Commands:
help-all
help
add
edit
del
listc
comp

Use help 'command_name' to get help about the command!

>>
```

To get help about a command:

```
>> help add
>>> command (add):
Description: Adds a note to the creator.
Usage: 'add {creatorId} {description}'
Example: 'add 387 My custom note'

>>
```

# **Testing phase**
 
## Tools used to make the tests

Different tools were in fact used to make the tests and are very useful to help debugging an application. ```REST-Assured```, which works on top of ```JUnit```; ```Hamcrest``` knowledge is also helpful since we also use Hamcrest matchers to test our responses. ```Gson``` is automatically used by REST-Assured for JSON (de)serialization, so we also need to have some knowledge on that. Actually, Gson is also used out of the test phase for minor things like pretty printing.

## Running the tests

When we build a project using ```$ mvn clean package``` all tests are run by default (like Unit and Integration tests). Full automation is good, however, this can be undesired sometimes like it's the case in this project, where we have Integration-tests that need to be run but are dependent of the ```.war``` that will only be generated ... after the test phase! The good news are we can configure this behavior in Maven and separate each phase of the build process, to avoid problems like these. If we don't, the build will fail, because the .war is not yet packaged and deployed in the web-server to receive the requests. The respective configuration in the ```pom.xml``` is shown below: 

```
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-failsafe-plugin</artifactId>
    <version>2.20</version>
    <executions>
        <execution>
            <goals>
                <goal>integration-test</goal>
                <goal>verify</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

Thus, in order to fire our Integration-tests, we will run the following command after deploying the .war to the server as previously depicted:

```
$ mvn verify
```

You will see something like these once the tests end:

```
(...)
 DEBUG [main]: (BasicClientConnectionManager.java:releaseConnection:198) - Releasing connection org.apache.http.impl.conn.ManagedClientConnectionImpl@2cf23c81
 DEBUG [main]: (BasicClientConnectionManager.java:releaseConnection:223) - Connection can be kept alive indefinitely
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.877 s - in com.pgeadas.notes.resources.POSTAndUPDATEAndDELETE_IT
[INFO]
[INFO] Results:
[INFO]
[INFO] Tests run: 6, Failures: 0, Errors: 0, Skipped: 0
[INFO]
[INFO]
[INFO] --- maven-failsafe-plugin:2.20:verify (default) @ MarvelSuperHeroesAPI ---
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 11.851 s
[INFO] Finished at: 2017-07-10T01:52:26+01:00
[INFO] Final Memory: 28M/294M
[INFO] ------------------------------------------------------------------------
```

#### **```IMPORTANT POSSIBLE ISSUE:```**  
####(this is very important because it can make some tests fail) - Remembered that the hashes generated during the tests used my keys... so, in order for some tests pass, the existing hash inside test files /src/test/resources must be changed to match the generated hash using your keys. Or if needed i can send my keys by email, no problem, just didnt want to upload them public.

## Test description

Some tests can and were done in separate, like testing if an endpoint is working or not. However, there are cases where the result of a 'next' test depends on a 'previous' test. For example in our App:

```
add 1 Note
add 1 Note 
``` 

Adding the same note to the same creator is not allowed and returns an error, so it is different to test the App for, let's call it responsiveness (both calls return a result and are executed successfully) or testing for correctness (the first Note is added successfully in the Database, but the second fails cause it's a duplicate), so there is really an immense set of testing possibilities here.
Example of a series of tests that test for both responsiveness and correctness of the App:

```
@Test
    public void testCreateAndUpdateAndDelete() throws IOException {
        //delete a note
        deleteNote();
        //adds a note
        testAddSuccess();
        //tries to add duplicate of the previous note
        testAddDuplicate();
        //tries to modify the note
        testUpdateSuccess();
        //tries to update wrong hash note
        testUpdateFail(invalidPutRequest);
        //since PUT is idempotent, lets try to update the valid note again, result will be always the same
        testUpdateSuccess();
        //Delete the note
        testDeleteSuccess();
        //tries to delete the same note again
        testDeleteFail();
        //tries to update the note that was deleted
        testUpdateFail(validPutRequest);
    }
```

# **Application Logging**

Application logging is very important, so we know what happened with each call to our App, who issued the requests and the result of the requested operation.
The configuration for the concrete logging framework used in the project ``` (log4j)```  is in the file ```../resources/log4j.properties```. Before building the App, one can change the folder where to save the logs or the level of logs to be displayed in the console, the type of logs, format for the logs, etc.


# **Frameworks and Libs used**

Here is a list of the external frameworks and libraries used in this project followed by brief description about them.

* [PostgreSQL](https://www.postgresql.org/) - A powerful open source object-relational database system with a strong reputation for reliability, data integrity, and correctness. It was used to store Notes on creators.
* [JUnit](http://junit.org/junit4/) - JUnit is a simple framework to write repeatable tests.
* [Rest-Assured](http://rest-assured.io/) - Testing and validating REST services in Java.
* [Hamcrest](http://hamcrest.org/) - Matchers that can be combined to create flexible expressions of intent to test our apps.
* [log4j](https://logging.apache.org/log4j/1.2/download.html) - Widely known and used framework used for logging.
* [slf4j](https://www.slf4j.org/) - "The Simple Logging Facade for Java (SLF4J) serves as a simple facade or abstraction for various logging frameworks (e.g. java.util.logging, logback, log4j) allowing the end user to plug in the desired logging framework at deployment time."
* [Jayway JsonPath](https://github.com/json-path/JsonPath) - A Java DSL for reading JSON documents. Used to extract the creator fields from the Marvel API response without the need to create any beans for that purpose. Also very useful to Filter Json objects with expressions, similarly as XPath is for XML.
* [org.everit.json.schema](https://github.com/everit-org/json-schema) - Json-schema validation library.  This lib was used because "it seems to be twice faster than the Jackson-based" one. It provides also very informative descriptions when exceptions are detected, which gives great feedback to users.
* [GSON](https://github.com/google/gson) - A Java serialization/deserialization library to convert Java Objects into JSON and back. Used to pretty print some strings as Json.
* [Jersey](https://jersey.github.io/) - Jersey RESTful Web Services framework is open source, production quality, framework for developing RESTful Web Services in Java. It provides support for JAX-RS APIs and serves as a JAX-RS (JSR 311 & JSR 339) Reference Implementation, that is why was used.
* [Maven](https://maven.apache.org/) - Apache Maven is a software project management and comprehension tool, very useful to deal with dependencies and build of projects.
* [Tomcat](https://tomcat.apache.org/tomcat-8.5-doc/index.html) - "Apache Tomcat version 8.5 implements the Servlet 3.1 and JavaServer Pages 2.3 specifications from the Java Community Process, and includes many additional features that make it a useful platform for developing and deploying web applications and web services."
* [ApacheCommons](https://commons.apache.org/) - Apache Commons is an Apache project focused on reusable Java components. A good example is the case of the 'ComparatorUtils.chainedComparator', that allows sorting by chaining a set of Comparators (this is how our sorting by various fields at a time is done)


# KNOWN ISSUES 

#### **```IMPORTANT ONE:```**  
#####this is very critical because ```$ mvn verify``` may fail because of this!) remembered that the hashes geenrated to to the tests used my keys... so i need to correct the tests to actually go to the properties.file and get the key instead of passing it as paramter

###### **```LESS IMPORTANT```**  

- For a reason of coherency, the endpoint to list notes should be renamed to /notes/list;
- Marvel API takes too much time to respond to certain requests, like the ones who *compare* things in common among all creators.  If we think about that, that is totally normal, since the amount of information to process must be huge. In order to speed up our API responsiveness, knowing that per *compare* request we need to call at least 4 endpoints in Marvel side, we should consider in having a pool of threads to perform jobs like this and speed up our app, like we have in the Client tool, for example.
- when cleaning up the code, just deleted too many debug lines (SQLConnector Im looking at you), the result is that we have not enough information to see what is happening with the current logs (with just a SQL query print, we know almost everything we are trying to do with our app, namely what we are getting, inserting, who is trying to do that. Just un-commented the line that prints the queries to db)
- forgot to add the status success or fail to the response when doing GetNoteByHash
- since add, del, edit need a body request, there is a schema for each request with details on how to construct a valid request. Those schemas are available in the resource folder, but it could be of use to have an endpoint to expose them to users of the API, so they are able to make valid requests to it if using the endpoints directly from a browser. 
- More performance related stuff, like a cache
- Add description in the message returned when it fails because the database = null

# Authors

* **Pedro Geadas** - [MyGitHubPage](https://github.com/pgeadas/)
