package com.pgeadas.notes.response;

import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.constants.ServiceCommons;
import com.pgeadas.notes.constants.ServiceNotesTags;
import com.pgeadas.notes.helpers.JSONObjectBuilder;
import static com.pgeadas.notes.helpers.JSONObjectBuilder.buildFromList;
import static com.pgeadas.notes.helpers.JSONObjectBuilder.buildFromListByCreatorId;
import com.pgeadas.notes.helpers.QueryParametersExtractor;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author geadas
 */
public class JSONResponse implements JSONObjectBuilder, QueryParametersExtractor {

    private static final Logger log = LoggerFactory.getLogger(JSONResponse.class);

    /**
     * @param genericObjectList Single note, response info still included to
     * tell if the note was found or not (count = 0 if not, 1 otherwise)
     * @return
     */
    public JSONObject createResponse(List<Note> genericObjectList) {
        JSONObject response = new JSONObject();
        JSONObject properties = new JSONObject();

        properties.put(ServiceCommons.countTag, genericObjectList.size());
        response.put(ServiceCommons.responseInfoTag, properties);
        //add the Notes to the response
        response.put(ServiceCommons.responseDataTag, buildFromList(genericObjectList));

        return response;
    }

    /**
     * Creates an object with information about the status of the operation
     *
     *
     * @param status
     * @param msg
     * @return
     */
    public JSONObject createResponse(String status, String msg) {
        JSONObject response = new JSONObject();
        JSONObject properties = new JSONObject();
        properties.put(ServiceCommons.statusMsgTag, msg);
        properties.put(ServiceCommons.statusTag, status);
        response.put(ServiceCommons.responseInfoTag, properties);

        return response;
    }

    /**
     * Creates an object with information about the status of the operation
     *
     *
     * @param status
     * @param msg
     * @return
     */
    public JSONObject createResponse(String status, JSONObject msg) {
        JSONObject response = new JSONObject();
        JSONObject properties = new JSONObject();
        properties.put(ServiceCommons.statusMsgTag, msg);
        properties.put(ServiceCommons.statusTag, status);
        response.put(ServiceCommons.responseInfoTag, properties);

        return response;
    }

    /**
     * Creates an object with information about the status of the operation
     *
     *
     * @param status
     * @param apiResponse
     * @return
     */
    public JSONObject createMarvelAPIResponse(String status, JSONObject apiResponse) {
        JSONObject response = new JSONObject();
        response.put(ServiceCommons.statusTag, status);
        response.put(ServiceCommons.responseInfoTag, apiResponse);

        return response;
    }

    /**
     * @param totalCount
     * @param genericObjectList
     *
     * @param resultCount
     * @param limitAndOffset
     * @param uri
     * @param queryParameters
     * @return
     */
    public JSONObject createResponse(long totalCount, List<Note> genericObjectList, long resultCount, Pair<Integer, Integer> limitAndOffset, String uri, HashMap<String, String> queryParameters) {
        JSONObject response = new JSONObject();
        JSONObject properties = new JSONObject();

        //remove the / if it is already there
        if (uri.charAt(uri.length() - 1) == '/') {
            uri = uri.substring(0, uri.length() - 1);
        }

        //checks additional parameters in the query to add to the commodity references
        String fields = QueryParametersExtractor.getSomeQueryParameters(queryParameters);

        // log.debug("fields: " + fields);
        //if total number of results is 0, there is no need for checking previous, next, first and last
        if (totalCount != 0) {
            //build the hrefs of "previous","next","first" and "last" for commodity
            buildFirstAndLast(properties, fields, uri, totalCount, resultCount, limitAndOffset);
            buildNext(properties, fields, uri, totalCount, resultCount, limitAndOffset);
            buildPrevious(properties, fields, uri, resultCount, limitAndOffset);
        }

        //add the response information
        properties.put(ServiceCommons.totalCountTag, totalCount);
        properties.put(ServiceCommons.countTag, resultCount);
        response.put(ServiceCommons.responseInfoTag, properties);

        //add the /, needed for building the note href
        uri += "/";

        if (queryParameters.containsKey(ServiceNotesTags.creatorIdTag)) {
            //if we are already specifying the creatorId, we only need to print creatorId and creatorName once
            response.put(ServiceCommons.responseDataTag, buildFromListByCreatorId(genericObjectList, uri, Integer.parseInt(queryParameters.get(ServiceNotesTags.creatorIdTag))));
        } else {
            //add all the Notes to the response
            response.put(ServiceCommons.responseDataTag, buildFromList(genericObjectList, uri));
        }

        return response;
    }

    //we can check if they are refering to the same page before adding them to properties if we wish
    private void buildFirstAndLast(JSONObject properties, String fields, String uri, long totalCount, long resultCount, Pair<Integer, Integer> limitAndOffset) {
        //if there are no results returned, add option to go back to the first and last page
        if (resultCount == 0) {
            long newTotal = totalCount - limitAndOffset.getLeft();
            properties.put(ServiceCommons.firstTag, ((uri.concat("?limit=") + limitAndOffset.getLeft()).concat("&offset=")
                    + 0).concat(fields));
            properties.put(ServiceCommons.lastTag, ((uri.concat("?limit=") + limitAndOffset.getLeft()).concat("&offset=")
                    + (newTotal > 0 ? newTotal : 0)).concat(fields));
        }
    }

    //we can check if they are refering to the same page before adding them to properties if we wish
    private void buildPrevious(JSONObject properties, String fields, String uri, long resultCount, Pair<Integer, Integer> limitAndOffset) {
        //check if we can navigate to a previous set off notes (if offset <= 0, we clearly cant)
        if (limitAndOffset.getRight() > 0) {
            int offsetDif = limitAndOffset.getRight() - limitAndOffset.getLeft();
            if (limitAndOffset.getRight() + limitAndOffset.getLeft() - resultCount > 0) {
                properties.put(ServiceCommons.previousTag, ((uri.concat("?limit=") + limitAndOffset.getLeft()).concat("&offset=")
                        + (offsetDif > 0 ? offsetDif : 0)).concat(fields));
            } else {
                properties.put(ServiceCommons.previousTag, "");
            }
        } else {
            properties.put(ServiceCommons.previousTag, "");
        }
    }

    //we can check if they are refering to the same page before adding them to properties if we wish
    private void buildNext(JSONObject properties, String fields, String uri, long totalCount, long resultCount, Pair<Integer, Integer> limitAndOffset) {
        //check if we can navigate to a next set off notes
        if (limitAndOffset.getRight() + resultCount < totalCount) {
            properties.put(ServiceCommons.nextTag, ((uri.concat("?limit=") + limitAndOffset.getLeft()).concat("&offset=")
                    + (limitAndOffset.getLeft() + limitAndOffset.getRight())).concat(fields));
        } else {
            properties.put(ServiceCommons.nextTag, "");
        }
    }

}
