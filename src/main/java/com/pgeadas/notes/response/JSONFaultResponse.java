package com.pgeadas.notes.response;

import org.json.JSONObject;

/**
 *
 * @author geadas
 */
public class JSONFaultResponse extends JSONResponse {

    /**
     * Creates an object with information about why the request failed.
     * Sometimes the request is processed correctly by our App, but fails while
     * calling external APIs, so the global result maybe a 200 OK even when some
     * error happens.
     *
     * @param msg
     * @return
     */
    public JSONObject createResponse(String msg) {
        return super.createResponse("Failed", msg);
    }

    public JSONObject createResponse(JSONObject msg) {
        return super.createResponse("Failed", msg);
    }
}
