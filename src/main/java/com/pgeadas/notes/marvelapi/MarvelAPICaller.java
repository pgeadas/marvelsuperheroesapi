package com.pgeadas.notes.marvelapi;

import com.jayway.jsonpath.JsonPathException;
import com.pgeadas.notes.constants.ServiceCommons;
import com.pgeadas.notes.constants.Functions;
import com.pgeadas.notes.constants.ServiceCreatorsTags;
import com.pgeadas.notes.database.strategy.DatabaseContext;
import com.pgeadas.notes.exceptions.BadParameterException;
import com.pgeadas.notes.exceptions.HttpCallException;
import com.pgeadas.notes.exceptions.JsonException;
import com.pgeadas.notes.helpers.JSONObjectBuilder;
import com.pgeadas.notes.helpers.QueryParametersExtractor;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author geadas
 *
 * This class defines the methods that will be used to make the calls to Marvel
 * API.
 */
public final class MarvelAPICaller implements QueryParametersExtractor, JSONObjectBuilder {

    private final static Logger log = LoggerFactory.getLogger(MarvelAPICaller.class);
    private final HttpCaller httpCaller;
    private ParserOperations parser;
    private HashMap<String, String> myCustomSortParameters, myCustomFilterParameters;

    /**
     *
     * @param parserType
     * @throws IOException
     * @throws JsonPathException
     * @throws JsonException
     */
    public MarvelAPICaller(ParserOperations parserType) throws IOException, JsonPathException, JsonException {
        log.debug("MarvelAPICaller initialising...");
        initParser(parserType);
        httpCaller = new HttpCaller();
        log.debug("MarvelAPICaller initialised.");
    }

    /**
     *
     * @return
     */
    public String getActualParserType() {
        return parser.getClass().toString();
    }

    private void initParser(ParserOperations parserInstance) throws IOException {
        parser = parserInstance;
    }

    private JSONObject getCreators(String toParse, HashMap<String, String> myCustomSortParameters, HashMap<String, String> myCustomFilterParameters, DatabaseContext dbContext, String username) throws JsonPathException, JsonException, BadParameterException, SQLException {
        parser.setContextReaderContext(toParse);
        return parser.getCreators(myCustomSortParameters, myCustomFilterParameters, dbContext, username);
    }

    private JSONObject getCompareCreators(String creator1, String creator2, Map<String, String> sharedResources, DatabaseContext dbContext, String username) throws JsonPathException, JsonException, BadParameterException, SQLException, HttpCallException {
        return parser.getCompareCreators(creator1, creator2, sharedResources, dbContext, username);
    }

    private String getCreatorName(String toParse) throws JsonPathException, JsonException, HttpCallException {
        parser.setContextReaderContext(toParse);
        return parser.getCreatorName();
    }

    /**
     *
     * @param username
     * @param marvelUri
     * @param allParameters
     * @param dbContext
     * @param ts
     * @param privateKey
     * @param publicKey
     * @return
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws JsonPathException
     * @throws JsonException
     * @throws HttpCallException
     * @throws SQLException
     * @throws BadParameterException
     */
    public JSONObject getCreatorsList(String username, String marvelUri, HashMap<String, String> allParameters, DatabaseContext dbContext, long ts, String privateKey, String publicKey) throws NoSuchAlgorithmException, IOException, JsonPathException, JsonException, HttpCallException, SQLException, BadParameterException {
        String hash = generateHash(ts, privateKey, publicKey);
        //separates myCustomSortParameters and myCustomFilterParameters from allParameters
        extractParameters(allParameters);
        String URL = buildGetCreatorsListURL(marvelUri, allParameters, hash, publicKey, ts);
        log.debug("getCreatorsList address: {}", URL);
        String result = httpCaller.getDefaultHttpResponse(URL);
        JSONObject creators = getCreators(result, myCustomSortParameters, myCustomFilterParameters, dbContext, username);
        return creators;
    }

    /**
     *
     * @param marvelUri
     * @param creatorId
     * @param ts
     * @param privateKey
     * @param publicKey
     * @return
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws JsonPathException
     * @throws JsonException
     * @throws HttpCallException
     * @throws SQLException
     * @throws BadParameterException
     */
    public String getCreatorName(String marvelUri, int creatorId, long ts, String privateKey, String publicKey) throws NoSuchAlgorithmException, IOException, JsonPathException, JsonException, HttpCallException, SQLException, BadParameterException {
        String hash = generateHash(ts, privateKey, publicKey);
        String URL = buildGetCreatorByIdURL(marvelUri, creatorId, hash, publicKey, ts);
        log.debug("getCreatorName address: {}", URL);
        String result = httpCaller.getDefaultHttpResponse(URL);
        String creator = getCreatorName(result);
        return creator;
    }

    /**
     *
     * @param marvelUri
     * @param id1
     * @param id2
     * @param dbContext
     * @param username
     * @param ts
     * @param privateKey
     * @param publicKey
     * @return
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws JsonPathException
     * @throws JsonException
     * @throws HttpCallException
     * @throws SQLException
     * @throws BadParameterException
     */
    public JSONObject getCompareCreators(String marvelUri, int id1, int id2, DatabaseContext dbContext, String username, long ts, String privateKey, String publicKey) throws NoSuchAlgorithmException, IOException, JsonPathException, JsonException, HttpCallException, SQLException, BadParameterException {
        String hash = generateHash(ts, privateKey, publicKey);
        String URL1 = buildGetCreatorByIdURL(marvelUri, id1, hash, publicKey, ts);
        String URL2 = buildGetCreatorByIdURL(marvelUri, id2, hash, publicKey, ts);
        log.debug("getCompareCreators id1: '{}'", URL1);
        log.debug("getCompareCreators id2: '{}'", URL2);

        //try to check which one failed to get here, cause when we call getCompareCreators it is certain that the call succeeded
        String creator1 = httpCaller.getDefaultHttpResponse(URL1);
        String creator2 = httpCaller.getDefaultHttpResponse(URL2);

        //////get common stuff
        //comics
        String comicsURL = buildSharedURL(ServiceCommons.MARVEL_API_COMICS_URI, ServiceCreatorsTags.creatorsTag, id1, id2, hash, publicKey, ts);
        log.debug("comicsURL '{}'", comicsURL);
        String commonComics = httpCaller.getDefaultHttpResponse(comicsURL);

        //series
        String seriesURL = buildSharedURL(ServiceCommons.MARVEL_API_SERIES_URI, ServiceCreatorsTags.creatorsTag, id1, id2, hash, publicKey, ts);
        log.debug("seriesURL series: '{}'", seriesURL);
        String commonSeries = httpCaller.getDefaultHttpResponse(seriesURL);

        //stories
        String storiesURL = buildSharedURL(ServiceCommons.MARVEL_API_STORIES_URI, ServiceCreatorsTags.creatorsTag, id1, id2, hash, publicKey, ts);
        log.debug("storiesURL: '{}'", storiesURL);
        String commonStories = httpCaller.getDefaultHttpResponse(storiesURL);

        //events
        String eventsURL = buildSharedURL(ServiceCommons.MARVEL_API_EVENTS_URI, ServiceCreatorsTags.creatorsTag, id1, id2, hash, publicKey, ts);
        log.debug("eventsURL '{}'", eventsURL);
        String commonEvents = httpCaller.getDefaultHttpResponse(eventsURL);

        //nr of comics in which they were collaborators
        String collaboratorsUrl = buildSharedURL(ServiceCommons.MARVEL_API_COMICS_URI, ServiceCreatorsTags.collaboratorsTag, id1, id2, hash, publicKey, ts);
        log.debug("collaboratorsURL '{}'", eventsURL);
        String collaborators = httpCaller.getDefaultHttpResponse(collaboratorsUrl);

        Map<String, String> sharedResources = new HashMap<>();
        sharedResources.put(ServiceCreatorsTags.commonComicsCountTag, commonComics);
        sharedResources.put(ServiceCreatorsTags.commonSeriesCountTag, commonSeries);
        sharedResources.put(ServiceCreatorsTags.commonStoriesCountTag, commonStories);
        sharedResources.put(ServiceCreatorsTags.commonEventsCountTag, commonEvents);
        sharedResources.put(ServiceCreatorsTags.collaboratorsTag, collaborators);

        JSONObject creators = getCompareCreators(creator1, creator2, sharedResources, dbContext, username);
        return creators;
    }

    /**
     * Returns a HashMap with the parameters used for further filtering and
     * sorting by this APP itself and not by the marvel API. If this parameters
     * are not extracted we will receive an error from the call to the API. The
     * parameters are removed from "allParameters" HashMap and added to the
     * returned map.
     *
     *
     * @param queryParameters
     * @return
     */
    private void extractParameters(HashMap<String, String> queryParameters) {
        myCustomSortParameters = new HashMap();
        myCustomFilterParameters = new HashMap();

        Iterator it = queryParameters.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            String key = (String) e.getKey();

            if (ServiceCreatorsTags.sortingTagExists(key)) {
                myCustomSortParameters.put(key, (String) e.getValue());
                it.remove();
            } else if (ServiceCreatorsTags.filteringTagExists(key)) {
                myCustomFilterParameters.put(key, (String) e.getValue());
                it.remove();
            }
        }
    }

    /**
     * Generate the hash needed to make the calls to the Marvel API.*
     */
    private String generateHash(long ts, String privateKey, String publicKey) throws NoSuchAlgorithmException {
        //add the components to generate the MD5 digest
        StringBuilder merger = new StringBuilder();
        merger.append(ts);
        merger.append(privateKey);
        merger.append(publicKey);
        //get the hash
        String hash = Functions.createMD5Digest(merger.toString());
        return hash;
    }

    /**
     * The next group of methods are used to generate the different URLS needed
     * to make the correct calls to Marvel API.*
     */
    private String buildGetCreatorsListURL(String marvelUri, HashMap<String, String> customParameters, String hash, String publicKey, long ts) {
        //add the components to build the URL
        StringBuilder merger = new StringBuilder();
        merger.append(marvelUri);
        appendMandatoryParameters(merger, hash, publicKey, ts);
        merger.append(QueryParametersExtractor.getAllQueryParameters(customParameters));
        return merger.toString();
    }

    private String buildGetCreatorByIdURL(String marvelUri, int id, String hash, String publicKey, long ts) {
        //add the components to build the URL
        StringBuilder merger = new StringBuilder();
        merger.append(marvelUri);
        merger.append("/");
        merger.append(id);
        appendMandatoryParameters(merger, hash, publicKey, ts);
        return merger.toString();
    }

    private String buildSharedURL(String marvelUri, String tag, int id1, int id2, String hash, String publicKey, long ts) {
        //add the components to build the URL
        StringBuilder merger = new StringBuilder();
        merger.append(marvelUri);
        appendMandatoryParameters(merger, hash, publicKey, ts);
        merger.append("&");
        merger.append(tag);
        merger.append("=");
        merger.append(id1);
        merger.append(",");
        merger.append(id2);
        return merger.toString();
    }

    /**
     * Appends the mandatory parameters to the URI.*
     */
    private void appendMandatoryParameters(StringBuilder merger, String hash, String publicKey, long ts) {
        merger.append("?apikey=");
        merger.append(publicKey);
        merger.append("&hash=");
        merger.append(hash);
        merger.append("&ts=");
        merger.append(ts);
    }

}
