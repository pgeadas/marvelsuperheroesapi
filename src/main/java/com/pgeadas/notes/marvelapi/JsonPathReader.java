package com.pgeadas.notes.marvelapi;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.JsonPathException;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.ReadContext;
import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.constants.ServiceCommons;
import com.pgeadas.notes.constants.ServiceCreatorsTags;
import com.pgeadas.notes.constants.ServiceNotesTags;
import com.pgeadas.notes.database.strategy.DatabaseContext;
import com.pgeadas.notes.exceptions.BadParameterException;
import com.pgeadas.notes.exceptions.HttpCallException;
import com.pgeadas.notes.exceptions.JsonException;
import com.pgeadas.notes.helpers.JSONObjectBuilder;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author geadas
 *
 * Used for parsing Strings and convert them to JSON representation that allows for
 * querying with expressions to find elements and filter as well. The library
 * used (JsonPath) is for Json like XPath is for XML.
 */
public final class JsonPathReader extends ParserOperations implements JSONObjectBuilder {

    private final static Logger log = LoggerFactory.getLogger(JsonPathReader.class);
    private final static Configuration CONFIGURATION = Configuration.builder().options(Option.DEFAULT_PATH_LEAF_TO_NULL).build(); //avoid null leafs
    private static ReadContext CTX;

    //tags to use with JsonPath and get the desired fields
    private final static String RESPONSE_CODE = "$.code";
    private final static String RESULTS_TAG = "$.data.results";
    private final static String RESULTS_FIRST_TAG = "$.data.results[0]";
    private final static String DATA_COUNT = "$.data.count";
    private final static String DATA_TOTAL_COUNT = "$.data.total";
    private final static String DATA_OFFSET = "$.data.offset";
    private final static String DATA_LIMIT = "$.data.limit";
    private final static String RESULTS_TAG_CREATOR_NAME = RESULTS_TAG.concat("[0].").concat(ServiceCreatorsTags.fullNameTag);
    private final static String RESULTS_TAG_ALL = RESULTS_TAG.concat("[*]");

    /**
     *
     */
    public JsonPathReader() {
    }

    /**
     * Creates a JsonPath context object when a request arrives, so we can read
     * multiple times from the object without the need of re-parsing it.
     *
     * This is done here instead of inside the method called, because there
     * might be situations where we can reuse the same context among calls, so
     * no need to be parsing again.
     *
     * @param marvelAPIResponseJSON
     */
    @Override
    protected void setContextReaderContext(String marvelAPIResponseJSON) {
        CTX = JsonPath.using(CONFIGURATION).parse(marvelAPIResponseJSON);
    }

    /**
     * Checks if a creator exists, depending on the response obtained from the
     * call to Marvel API
     *
     * @return
     * @throws com.pgeadas.notes.exceptions.JsonException
     * @throws com.pgeadas.notes.exceptions.HttpCallException
     */
    @Override
    public String getCreatorName() throws JsonPathException, JsonException, HttpCallException {
        int code = CTX.read(RESPONSE_CODE);
        String name;
        if (code == HttpStatus.SC_OK) {
            name = CTX.read(RESULTS_TAG_CREATOR_NAME);
        } else {
            throw new HttpCallException("Could not find that Creator!", code);
        }

        return name;
    }

    /**
     * @param creator1json
     * @param creator2json
     * @param sharedResources
     * @param dbContext
     * @param username
     * @return
     * @throws JsonPathException
     * @throws com.pgeadas.notes.exceptions.JsonException
     * @throws HttpCallException
     * @throws java.sql.SQLException
     * @throws com.pgeadas.notes.exceptions.BadParameterException
     */
    @Override
    public JSONObject getCompareCreators(String creator1json, String creator2json, Map<String, String> sharedResources, DatabaseContext dbContext, String username) throws JsonPathException, JsonException, HttpCallException, SQLException, BadParameterException {

        JSONObject apiResponse = new JSONObject();
        JSONArray creators = new JSONArray();
        JSONObject shared = new JSONObject();

        //read both strings to json objects so we can filter common attributes between them
        Map<String, Object> creator1 = getCreatorAsMap(creator1json);
        Map<String, Object> creator2 = getCreatorAsMap(creator2json);

        //log.debug(creator1.toString());
        //log.debug(creator2.toString());
        //puts both in a map, so we can use the same method to get the Notes from Database
        List<Map<String, Object>> creatorsList = new LinkedList();
        creatorsList.add(creator1);
        creatorsList.add(creator2);

        //we need to get the notes from the database here, before we do the sorting and append them to the List in the respective creator
        getCreatorsNotesFromDatabase(creatorsList, Optional.empty(), username, dbContext);

        //creates a JSONObject from the list of creators
        buildCreatorsListJSONObjectForSummary(creatorsList, creators);

        //creates a JSONObject with the #of shared resources
        buildSharedResourcesJSONObjectForSummary(sharedResources, shared);

        apiResponse.put(ServiceCreatorsTags.creatorsResultsTag, creators);
        apiResponse.put(ServiceCreatorsTags.commonResourceCountTag, shared);

        return apiResponse;
    }

    private Map getCreatorAsMap(String toParse) {
        return JsonPath.using(CONFIGURATION).parse(toParse).read(RESULTS_FIRST_TAG, Map.class);
    }

    /**
     * @param customSortParameters
     * @param customFilterParameters
     * @param dbContext
     * @param username
     * @return
     * @throws com.pgeadas.notes.exceptions.JsonException
     * @throws com.pgeadas.notes.exceptions.BadParameterException
     * @throws java.sql.SQLException
     */
    @Override
    public JSONObject getCreators(HashMap<String, String> customSortParameters, Map<String, String> customFilterParameters, DatabaseContext dbContext, String username) throws JsonPathException, JsonException, ClassCastException, BadParameterException, SQLException {
        JSONObject apiResponse = new JSONObject();
        JSONArray creators = new JSONArray();

        int count, totalCount, limit, offset;

        List<Map<String, Object>> creatorsList;

        //do filter by customParameters if they exist, otherwise gets the list of all returned creators
        if (customFilterParameters.isEmpty()) {
            count = CTX.read(DATA_COUNT);
            totalCount = CTX.read(DATA_TOTAL_COUNT);
            limit = CTX.read(DATA_LIMIT);
            offset = CTX.read(DATA_OFFSET);
            apiResponse.put(ServiceCreatorsTags.countTag, count);
            apiResponse.put(ServiceCreatorsTags.totalCountTag, totalCount);
            apiResponse.put(ServiceCreatorsTags.limitTag, limit);
            apiResponse.put(ServiceCreatorsTags.offsetTag, offset);
            creatorsList = CTX.read(RESULTS_TAG_ALL, List.class);
        } else {
            String query = validateAndFilterCreatorsQueryParameters(customFilterParameters); //Ver se nao era para adicionar mais parametros extra
            creatorsList = CTX.read(query, List.class);
            totalCount = CTX.read(DATA_TOTAL_COUNT);
            limit = CTX.read(DATA_LIMIT);
            offset = CTX.read(DATA_OFFSET);
            //the count is the nr of results returned after the filter
            apiResponse.put(ServiceCreatorsTags.countTag, creatorsList.size());
            //customFilteredCount is the nr of results that were in fact analized
            apiResponse.put(ServiceCreatorsTags.customFilteredResultsTag, limit);
            apiResponse.put(ServiceCreatorsTags.totalCountTag, totalCount);
            apiResponse.put(ServiceCreatorsTags.limitTag, limit);
            apiResponse.put(ServiceCreatorsTags.offsetTag, offset);
        }

        //we need to get the notes from the database here, before we do the sorting and append them to the List in the respective creator
        getCreatorsNotesFromDatabase(creatorsList, customSortParameters, username, dbContext);

        //sorts the resulting list
        validateAndSortCreatorsQueryParameters(customSortParameters, creatorsList);

        //creates a JSONObject from the list of creators
        buildCreatorsListJSONObject(creatorsList, creators);

        apiResponse.put(ServiceCreatorsTags.creatorsResultsTag, creators);
        return apiResponse;
    }

    private void buildSharedResourcesJSONObjectForSummary(Map<String, String> sharedResources, JSONObject shared) {
        Iterator it = sharedResources.keySet().iterator();
        try {
            int totalCount;
            while (it.hasNext()) {
                String key = (String) it.next();
                totalCount = JsonPath.read(sharedResources.get(key), DATA_TOTAL_COUNT);
                shared.put(key, totalCount);
            }

        } catch (com.jayway.jsonpath.InvalidPathException ex) {
            log.error("Error executing JsonPath Query: '{}'", ex.getMessage());
            throw new JsonPathException(ex.getMessage());
        }

    }

    private void buildCreatorsListJSONObjectForSummary(List<Map<String, Object>> creatorsList, JSONArray creators) {
        try {
            for (int i = 0; i < creatorsList.size(); i++) {
                int creatorId = (int) creatorsList.get(i).get(ServiceCreatorsTags.idTag);
                Map customNotesMap = (Map) creatorsList.get(i).get(ServiceCreatorsTags.customNotesTag);
                creators.put(
                        JSONObjectBuilder.buildCreatorForSummary(
                                creatorId,
                                (String) creatorsList.get(i).get(ServiceCreatorsTags.firstNameTag),
                                (String) creatorsList.get(i).get(ServiceCreatorsTags.middleNameTag),
                                (String) creatorsList.get(i).get(ServiceCreatorsTags.lastNameTag),
                                (String) creatorsList.get(i).get(ServiceCreatorsTags.modifiedTag),
                                (int) ((Map) creatorsList.get(i).get(ServiceCreatorsTags.comicsTag)).get(ServiceCreatorsTags.innerValueTag),
                                (int) ((Map) creatorsList.get(i).get(ServiceCreatorsTags.eventsTag)).get(ServiceCreatorsTags.innerValueTag),
                                (int) ((Map) creatorsList.get(i).get(ServiceCreatorsTags.storiesTag)).get(ServiceCreatorsTags.innerValueTag),
                                (int) ((Map) creatorsList.get(i).get(ServiceCreatorsTags.seriesTag)).get(ServiceCreatorsTags.innerValueTag),
                                JSONObjectBuilder.buildCreatorCustomNotes((long) customNotesMap.get(ServiceNotesTags.notesCountTag), (List) customNotesMap.get(ServiceNotesTags.notesTag))
                        ));

            }

        } catch (com.jayway.jsonpath.InvalidPathException ex) {
            log.error("Error executing JsonPath Query: '{}'", ex.getMessage());
            throw new JsonPathException(ex.getMessage());
        }
    }

    private void buildCreatorsListJSONObject(List<Map<String, Object>> creatorsList, JSONArray creators) {
        try {
            for (int i = 0; i < creatorsList.size(); i++) {
                int creatorId = (int) creatorsList.get(i).get(ServiceCreatorsTags.idTag);
                Map customNotesMap = (Map) creatorsList.get(i).get(ServiceCreatorsTags.customNotesTag);
                creators.put(
                        JSONObjectBuilder.buildCreator(
                                creatorId,
                                (String) creatorsList.get(i).get(ServiceCreatorsTags.fullNameTag),
                                (String) creatorsList.get(i).get(ServiceCreatorsTags.modifiedTag),
                                (int) ((Map) creatorsList.get(i).get(ServiceCreatorsTags.comicsTag)).get(ServiceCreatorsTags.innerValueTag),
                                (int) ((Map) creatorsList.get(i).get(ServiceCreatorsTags.eventsTag)).get(ServiceCreatorsTags.innerValueTag),
                                (int) ((Map) creatorsList.get(i).get(ServiceCreatorsTags.storiesTag)).get(ServiceCreatorsTags.innerValueTag),
                                (int) ((Map) creatorsList.get(i).get(ServiceCreatorsTags.seriesTag)).get(ServiceCreatorsTags.innerValueTag),
                                JSONObjectBuilder.buildCreatorCustomNotes((long) customNotesMap.get(ServiceNotesTags.notesCountTag), (List) customNotesMap.get(ServiceNotesTags.notesTag))
                        ));

            }

        } catch (com.jayway.jsonpath.InvalidPathException ex) {
            log.error("Error executing JsonPath Query: '{}'", ex.getMessage());
            throw new JsonPathException(ex.getMessage());
        }
    }

    private void getCreatorsNotesFromDatabase(List<Map<String, Object>> creatorsList, HashMap<String, String> customParameters, String username, DatabaseContext dbContext) throws SQLException, BadParameterException {
        Iterator it = creatorsList.iterator();
        while (it.hasNext()) {
            Map<String, Object> creator = ((Map) it.next());
            int creatorId = (int) creator.get(ServiceCreatorsTags.idTag);
            Pair<Long, List<Note>> pair = dbContext.getNoteS(username, Pair.of(ServiceCommons.DEFAULT_LIMIT, ServiceCommons.DEFAULT_OFFSET), Optional.of(customParameters), Optional.of(creatorId));
            Map<String, Object> customNotes = new HashMap<>();
            customNotes.put(ServiceNotesTags.notesCountTag, pair.getLeft());
            customNotes.put(ServiceNotesTags.notesTag, pair.getRight());
            creator.put(ServiceCreatorsTags.customNotesTag, customNotes);
        }
    }

    private void getCreatorsNotesFromDatabase(List<Map<String, Object>> creatorsList, Optional<HashMap<String, String>> customParameters, String username, DatabaseContext dbContext) throws SQLException, BadParameterException {
        Iterator it = creatorsList.iterator();
        while (it.hasNext()) {
            Map<String, Object> creator = ((Map) it.next());
            int creatorId = (int) creator.get(ServiceCreatorsTags.idTag);
            Pair<Long, List<Note>> pair = dbContext.getNoteS(username, Pair.of(ServiceCommons.DEFAULT_LIMIT, ServiceCommons.DEFAULT_OFFSET), customParameters, Optional.of(creatorId));
            Map<String, Object> customNotes = new HashMap<>();
            customNotes.put(ServiceNotesTags.notesCountTag, pair.getLeft());
            customNotes.put(ServiceNotesTags.notesTag, pair.getRight());
            creator.put(ServiceCreatorsTags.customNotesTag, customNotes);
        }
    }

    /**
     * Builds a JsonPath query to filter the results by our custom parameters. *
     * *
     *
     * TODO: Add option to allow for greater than and less than operations on
     * Integer params... One easy way to do so is adding "gt" and "lt" to the
     * Value providaded in the Query, than try to cast the value to Integer. If
     * it fails, we try to get the tag. If the tag is one of the expected
     * strings (lt or gt), use it. If its not any of these, it is an invalid
     * value.*
     */
    private String validateAndFilterCreatorsQueryParameters(Map<String, String> customFilterParameters) throws BadParameterException {
        String valueParam;
        StringBuilder query = new StringBuilder(RESULTS_TAG);
        query.append("[?(");

        Iterator it = customFilterParameters.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String key = (String) entry.getKey();

            if (ServiceCreatorsTags.filteringTagExists(key)) {
                valueParam = (String) entry.getValue();
                switch (key) {
                    case ServiceCreatorsTags.filterFullNameEqualsTag:
                        appendEqualsFilter(ServiceCreatorsTags.fullNameTag, valueParam, query);
                        break;
                    case ServiceCreatorsTags.filterFullNameContainsTag:
                        appendRegexFilter(ServiceCreatorsTags.fullNameTag, valueParam, query);
                        break;
                    case ServiceCreatorsTags.filterComicsNrTag:
                        appendEqualsFilter(ServiceCreatorsTags.getFilteringTagValue(ServiceCreatorsTags.filterComicsNrTag), valueParam, query);
                        break;
                    case ServiceCreatorsTags.filterEventsNrTag:
                        appendEqualsFilter(ServiceCreatorsTags.getFilteringTagValue(ServiceCreatorsTags.filterEventsNrTag), valueParam, query);
                        break;
                    case ServiceCreatorsTags.filterSeriesNrETag:
                        appendEqualsFilter(ServiceCreatorsTags.getFilteringTagValue(ServiceCreatorsTags.filterSeriesNrETag), valueParam, query);
                        break;
                    case ServiceCreatorsTags.filterStoriesNrTag:
                        appendEqualsFilter(ServiceCreatorsTags.getFilteringTagValue(ServiceCreatorsTags.filterStoriesNrTag), valueParam, query);
                        break;
                    default:
                        break;
                }
            }

            if (it.hasNext()) {
                query.append(" && ");
            } else {
                query.append(")]");
            }
        }

        return query.toString();

    }

    //jsonpath filters
    private void appendEqualsFilter(String keyParam, String valueParam, StringBuilder query) {
        query.append("(@.");
        query.append(keyParam);
        query.append(" == ");
        query.append(valueParam);
        query.append(")");
    }

    //jsonpath filters
    private void appendRegexFilter(String keyParam, String valueParam, StringBuilder query) {
        query.append("(@.");
        query.append(keyParam);
        query.append(" =~ /.*");
        valueParam = valueParam.replace(".", "\\.");
        query.append(valueParam);
        query.append(".*/i)");
    }

    /**
     * If it exists and has the expected value, do the sort. Else, do nothing
     * because it may be a tag available for Marvel API service itself. If it
     * exists and has an invalid value, throw Exception.*
     */
    private void validateAndSortCreatorsQueryParameters(HashMap<String, String> customFilterParameters, List<Map<String, Object>> creatorsList) throws BadParameterException {
        List<Comparator> comparators = new LinkedList();
        for (Map.Entry e : customFilterParameters.entrySet()) {
            String key = (String) e.getKey();
            String valueParam;

            if (ServiceCreatorsTags.sortingTagExists(key)) {
                valueParam = (String) e.getValue();
                switch (key) {
                    case ServiceCreatorsTags.sortById:
                        sortByOuter(valueParam, ServiceCreatorsTags.idTag, comparators);
                        break;
                    case ServiceCreatorsTags.sortByFullName:
                        sortByOuter(valueParam, ServiceCreatorsTags.fullNameTag, comparators);
                        break;
                    case ServiceCreatorsTags.sortByModified:
                        sortByOuter(valueParam, ServiceCreatorsTags.modifiedTag, comparators);
                        break;
                    case ServiceCreatorsTags.sortByComicsNr:
                        sortByInner(valueParam, ServiceCreatorsTags.comicsTag, ServiceCreatorsTags.innerValueTag, comparators);
                        break;
                    case ServiceCreatorsTags.sortByStoriesNr:
                        sortByInner(valueParam, ServiceCreatorsTags.storiesTag, ServiceCreatorsTags.innerValueTag, comparators);
                        break;
                    case ServiceCreatorsTags.sortBySeriesNr:
                        sortByInner(valueParam, ServiceCreatorsTags.seriesTag, ServiceCreatorsTags.innerValueTag, comparators);
                        break;
                    case ServiceCreatorsTags.sortByEventsNr:
                        sortByInner(valueParam, ServiceCreatorsTags.eventsTag, ServiceCreatorsTags.innerValueTag, comparators);
                        break;
                    case ServiceCreatorsTags.sortByNotesNr:
                        sortByInner(valueParam, ServiceCreatorsTags.customNotesTag, ServiceNotesTags.notesCountTag, comparators);
                    default:
                        break;
                }
            }
        }

        /**
         * make the sorting if there are any comparators to use. We use Apache
         * chainedComparator to sort by more than one field and maintain the
         * grouping of the previous one.
         */
        if (!comparators.isEmpty()) {
            Collections.sort(creatorsList, ComparatorUtils.chainedComparator(comparators));
        }

    }

    /**
     * Adds the respective comparator to the list if the parameter is valid or
     * throw exception if it is not valid.*
     */
    private void sortByOuter(String valueParam, String fieldName, List<Comparator> comparators) throws BadParameterException {
        if (valueParam.equalsIgnoreCase(ServiceCreatorsTags.sortAscending)) {
            comparators.add(new CustomOuterComparator(fieldName).ascendingOrder);
        } else if (valueParam.equalsIgnoreCase(ServiceCreatorsTags.sortDescending)) {
            comparators.add(new CustomOuterComparator(fieldName).descendingOrder);
        } else {
            throw new BadParameterException(String.format("Value parameter '%s' is not recognised!", valueParam));
        }
    }

    /**
     * Adds the respective comparator to the list if the parameter is valid or
     * throw exception if it is not valid.*
     */
    private void sortByInner(String valueParam, String fieldOuterName, String fieldInnerName, List<Comparator> comparators) throws BadParameterException {
        if (valueParam.equalsIgnoreCase(ServiceCreatorsTags.sortAscending)) {
            comparators.add(new CustomInnerComparator(fieldOuterName, fieldInnerName).ascendingOrder);
        } else if (valueParam.equalsIgnoreCase(ServiceCreatorsTags.sortDescending)) {
            comparators.add(new CustomInnerComparator(fieldOuterName, fieldInnerName).descendingOrder);
        } else {
            throw new BadParameterException(String.format("Value parameter '%s' is not recognised!", valueParam));
        }
    }

}

/**
 ************** Comparators used to sort the creators
 *
 * This comparators will get the parameter we want to compare from the Map. The
 * inner one gets an element that is one level deeper than the outer, hence the
 * names of the comparators. * Also, since they receive a Map of Objects, we try
 * to cast them to the most common type and if it fails, we will cast them to
 * the alternative type that we know the value might have.
 *
 */
class CustomInnerComparator {

    private final String keyOuter;
    private final String keyInner;

    CustomInnerComparator(String keyOuter, String keyInner) {
        this.keyOuter = keyOuter;
        this.keyInner = keyInner;
    }

    //comparator used to sort the list by the indicated key descending order
    Comparator<Map<String, Object>> descendingOrder = (Map<String, Object> o1, Map<String, Object> o2) -> {
        try {
            return intElements(o2, o1);
        } catch (Exception ex) {
            return longElements(o2, o1);
        }
    };

    //ascending order 
    Comparator<Map<String, Object>> ascendingOrder = (Map<String, Object> o1, Map<String, Object> o2) -> {
        try {
            return intElements(o1, o2);
        } catch (Exception ex) {
            return longElements(o1, o2);
        }
    };

    private int intElements(Map<String, Object> o1, Map<String, Object> o2) {
        int i = (int) ((Map) o1.get(keyOuter)).get(keyInner);
        int j = (int) ((Map) o2.get(keyOuter)).get(keyInner);

        if (i > j) {
            return 1;
        } else if (i < j) {
            return -1;
        } else {
            return 0;
        }
    }

    private int longElements(Map<String, Object> o1, Map<String, Object> o2) {
        long i = (long) ((Map) o1.get(keyOuter)).get(keyInner);
        long j = (long) ((Map) o2.get(keyOuter)).get(keyInner);

        if (i > j) {
            return 1;
        } else if (i < j) {
            return -1;
        } else {
            return 0;
        }
    }
}

class CustomOuterComparator {

    private final String key;

    CustomOuterComparator(String key) {
        this.key = key;
    }

    //comparator used to sort the list by the indicated key descending order
    Comparator<Map<String, Object>> descendingOrder = (Map<String, Object> o1, Map<String, Object> o2) -> {

        try {
            return intElements(o2, o1);
        } catch (Exception ex) {
            return stringElements(o2, o1);
        }

    };

    //ascending order
    Comparator<Map<String, Object>> ascendingOrder = (Map<String, Object> o1, Map<String, Object> o2) -> {

        try {
            return intElements(o1, o2);
        } catch (Exception ex) {
            return stringElements(o1, o2);
        }

    };

    private int intElements(Map<String, Object> o1, Map<String, Object> o2) {
        int i = (int) o1.get(key);
        int j = (int) o2.get(key);

        if (i > j) {
            return 1;
        } else if (i < j) {
            return -1;
        } else {
            return 0;
        }
    }

    private int stringElements(Map<String, Object> o1, Map<String, Object> o2) {
        String i = (String) o1.get(key);
        String j = (String) o2.get(key);

        int result = i.compareTo(j);

        if (result > 0) {
            return 1;
        } else if (result < 0) {
            return -1;
        } else {
            return 0;
        }
    }
}
