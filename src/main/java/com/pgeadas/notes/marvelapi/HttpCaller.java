package com.pgeadas.notes.marvelapi;

import com.pgeadas.notes.exceptions.HttpCallException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author geadas
 *
 * Class used to make HTTP requests.
 */
public class HttpCaller {

    private final String method;
    private final String contentType;
    private final int connectTimeout;

    /**
     *
     * @param method
     * @param contentType
     * @param connectTimeout
     */
    public HttpCaller(String method, String contentType, int connectTimeout) {
        this.method = method;
        this.contentType = contentType;
        this.connectTimeout = connectTimeout;
    }

    /**
     * Default options set.
     */
    public HttpCaller() {
        this.method = "GET";
        this.contentType = "application/json";
        this.connectTimeout = 5000;
    }

    /**
     * Makes an HTTP Request with the parameters set when this Class was
     * instantiated.
     *
     *
     * @param urlStr
     * @return
     * @throws java.io.IOException
     * @throws com.pgeadas.notes.exceptions.HttpCallException
     */
    public String getDefaultHttpResponse(String urlStr) throws IOException, HttpCallException {
        return getHttpResponse(urlStr, method, contentType, connectTimeout);
    }

    /**
     * Method used to call the Marvel API and retrieve the response from the
     * service
     *
     * @param urlStr
     * @param method
     * @param contentType
     * @param connectTimeout
     * @return
     * @throws java.io.IOException TODO: put this hard coded properties in the
     * properties file
     * @throws com.pgeadas.notes.exceptions.HttpCallException
     */
    public String getHttpResponse(String urlStr, String method, String contentType, int connectTimeout) throws IOException, HttpCallException {
        boolean error = false;
        URL url = new URL(urlStr);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream is;
        connection.setRequestMethod(method);
        connection.setRequestProperty("Accept", contentType);
        connection.setConnectTimeout(connectTimeout);
        connection.setDoOutput(true);

        try {
            //its ok
            is = connection.getInputStream();
        } catch (IOException ex) {
            //error
            is = connection.getErrorStream();
            error = true;
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (is)));

        StringBuilder outputBuilder = new StringBuilder();
        String output;
        while ((output = br.readLine()) != null) {
            outputBuilder.append(output);
        }
        connection.disconnect();

        String result = outputBuilder.toString();

        //we get the error message before throwing the exception
        if (error) {
            throw new HttpCallException(result, connection.getResponseCode());
        }

        return result;
    }
}
