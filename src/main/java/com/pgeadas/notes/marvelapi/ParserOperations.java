package com.pgeadas.notes.marvelapi;

import com.jayway.jsonpath.JsonPathException;
import com.pgeadas.notes.database.strategy.DatabaseContext;
import com.pgeadas.notes.exceptions.BadParameterException;
import com.pgeadas.notes.exceptions.HttpCallException;
import com.pgeadas.notes.exceptions.JsonException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/**
 *
 * @author geadas
 */
public abstract class ParserOperations {

    /**
     *
     * @param jsonToParse
     */
    protected abstract void setContextReaderContext(String jsonToParse);

    /**
     *
     * @param customSortParameters
     * @param customFilterParameters
     * @param dbContext
     * @param username
     * @return
     * @throws JsonPathException
     * @throws JsonException
     * @throws BadParameterException
     * @throws SQLException
     */
    public abstract JSONObject getCreators(HashMap<String, String> customSortParameters, Map<String, String> customFilterParameters, DatabaseContext dbContext, String username) throws JsonPathException, JsonException, BadParameterException, SQLException;

    /**
     *
     * @return
     * @throws JsonPathException
     * @throws JsonException
     * @throws HttpCallException
     */
    public abstract String getCreatorName() throws JsonPathException, JsonException, HttpCallException;

    /**
     *
     * @param creator1
     * @param creator2
     * @param sharedResources
     * @param dbContext
     * @param username
     * @return
     * @throws JsonPathException
     * @throws JsonException
     * @throws HttpCallException
     * @throws BadParameterException
     * @throws SQLException
     */
    public abstract JSONObject getCompareCreators(String creator1, String creator2, Map<String, String> sharedResources, DatabaseContext dbContext, String username) throws JsonPathException, JsonException, HttpCallException, BadParameterException, SQLException;

}
