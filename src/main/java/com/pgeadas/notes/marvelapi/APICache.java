package com.pgeadas.notes.marvelapi;

/**
 * TODO:... Useful to cache Notes from database or even possibly Creators from
 * the Marvel API. Since the "compare" process can be very slow, it might be
 * clever to store at least the info about the individual creators searched.
 *
 */
//package com.pgeadas.notes.jsonpath;
//
//import com.google.common.cache.CacheBuilder;
//import com.google.common.cache.CacheLoader;
//import com.google.common.cache.LoadingCache;
//import com.pgeadas.notes.beans.beans.Creator;
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Optional;
//import java.util.concurrent.TimeUnit;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
///**
// *
// * @author geadas
// */
//public class APICache {
//
//    private final static Logger log = LoggerFactory.getLogger(APICache.class);
//    private static ParserContext parserContext;
//    //50 pages of 20 (by default) Creators = 1000 Creators in cache at max
//    private static int DEFAULT_SIZE = 50;
//
//    public APICache(ParserContext actualParserContext, int size) {
//        parserContext = actualParserContext;
//        DEFAULT_SIZE = size;
//    }
//
//    public APICache(ParserContext actualParserContext) {
//        parserContext = actualParserContext;
//    }
//
//    public String getActualParserType() {
//        return parserContext.getActualParserType();
//    }
//
//    public static final LoadingCache<Integer, Optional<HashMap<Integer, Creator>>> cache = CacheBuilder.newBuilder()
//            .maximumSize(DEFAULT_SIZE)
//            .expireAfterAccess(24, TimeUnit.HOURS)
//            .recordStats()
//            .build(new CacheLoader<Integer, Optional<HashMap<Integer, Creator>>>() {
//                @Override
//                public Optional<HashMap<Integer, Creator>> load(Integer id) throws IOException {
//                    return getCreatorDetailsFromMarvelAPI(id);
//                }
//            });
//
//    /**
//     * Gets an HashMap of creators, corresponding to a "page" of Creators
//     * retrieved from Marvel API (I call a "page" of creators to the set of
//     * results returned by each call to the API, which is by default 20
//     * creators)*
//     */
//    private static Optional<HashMap<Integer, Creator>> getCreatorDetailsFromMarvelAPI(Integer id) {
//        return Optional.of(null);
//    }
//
//}
