package com.pgeadas.notes.beans.beans;

import java.sql.Timestamp;

/**
 *
 * @author geadas
 */
public class Note {

    private String hash;
    //not required, but interesting functionality to have for the future
    //private List<Tag> tags;
    private Timestamp creationDate;
    private Timestamp modificationDate;
    private String description;
    private String username;
    private String creatorName;
    private int creatorId;

    public Note() {
    }

    public Note(String hash) {
        this.hash = hash;
    }

    public Note(String description, String hash) {
        this.description = description;
        this.hash = hash;
    }

    public Note(String description, int creatorId) {
        this.description = description;
        this.creatorId = creatorId;
    }

    public Note(String description, String hash, String creatorName) {
        this.description = description;
        this.hash = hash;
        this.creatorName = creatorName;
    }

    public Note(String hash, Timestamp creationDate, Timestamp modificationDate, String description, String creatorName, int creatorId) {
        this.hash = hash;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
        this.description = description;
        this.creatorName = creatorName;
        this.creatorId = creatorId;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Timestamp getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Timestamp modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Note{" + "hash=" + hash + ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + ", description=" + description + ", creatorName=" + creatorName + '}';
    }

}
