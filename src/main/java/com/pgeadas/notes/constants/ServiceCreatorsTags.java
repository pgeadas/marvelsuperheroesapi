package com.pgeadas.notes.constants;

import java.util.HashMap;

/**
 *
 * @author geadas
 */
public final class ServiceCreatorsTags implements ServiceCommons {

    //tags for the compare creators service
    public static final String creatorId1Tag = "Id1";
    public static final String creatorId2Tag = "Id2";
    //sharedAppearancesTag commonComics
    public static final String creatorsTag = "creators";
    public static final String commonResourceCountTag = "commonResourceCount";
    public static final String collaboratorsTag = "collaborators";
    public static final String commonComicsCountTag = "commonComicsCount";
    public static final String commonEventsCountTag = "commonEventsCount";
    public static final String commonStoriesCountTag = "commonStoriesCount";
    public static final String commonSeriesCountTag = "commonSeriesCount";

    //available tags for sorting of creators
    public static final String sortById = "sortById";
    public static final String sortByFullName = "sortByFullName";
    public static final String sortByModified = "sortByModified";
    public static final String sortByComicsNr = "sortByComicsNr";
    public static final String sortByStoriesNr = "sortByStoriesNr";
    public static final String sortBySeriesNr = "sortBySeriesNr";
    public static final String sortByEventsNr = "sortByEventsNr";
    public static final String sortByNotesNr = "sortByNotesNr";

    //parameters tags (names of the parameters in marvel API) 
    public static final String idTag = "id";
    public static final String fullNameTag = "fullName";
    public static final String firstNameTag = "firstName";
    public static final String middleNameTag = "middleName";
    public static final String lastNameTag = "lastName";
    public static final String modifiedTag = "modified";
    public static final String comicsTag = "comics";
    public static final String storiesTag = "stories";
    public static final String seriesTag = "series";
    public static final String eventsTag = "events";
    //notes parameter tag, which will be added to the final response
    public static final String customNotesTag = "customNotes";
    public static final String customFilteredResultsTag = "customFilteredResults";
    public static final String creatorsResultsTag = "creators";

    //filtering tags (names of the parameters in marvel API) 
    public static final String filterFullNameContainsTag = "fullNameContains";
    public static final String filterFullNameEqualsTag = "fullNameEquals";
    public static final String filterComicsNrTag = "comicsNr";
    public static final String filterStoriesNrTag = "storiesNr";
    public static final String filterEventsNrTag = "eventsNr";
    public static final String filterSeriesNrETag = "seriesNr";

    //how to get the inner values
    public static final String innerValueTag = "available";

    //how to sort
    public static final String sortAscending = "ASC";
    public static final String sortDescending = "DESC";

    private static final HashMap<String, String> sortingTags = new HashMap<String, String>();
    private static final HashMap<String, String> filteringTags = new HashMap<String, String>();

    public ServiceCreatorsTags() {
        //sorting tags
        sortingTags.put(sortById, "");
        sortingTags.put(sortByFullName, "");
        sortingTags.put(sortByNotesNr, "");
        sortingTags.put(sortByModified, "");
        sortingTags.put(sortByComicsNr, "");
        sortingTags.put(sortByStoriesNr, "");
        sortingTags.put(sortBySeriesNr, "");
        sortingTags.put(sortByEventsNr, "");

        //filtering tags
        filteringTags.put(filterFullNameContainsTag, "");
        filteringTags.put(filterFullNameEqualsTag, "");
        filteringTags.put(filterComicsNrTag, comicsTag.concat(".").concat(innerValueTag));
        filteringTags.put(filterStoriesNrTag, storiesTag.concat(".").concat(innerValueTag));
        filteringTags.put(filterEventsNrTag, eventsTag.concat(".").concat(innerValueTag));
        filteringTags.put(filterSeriesNrETag, seriesTag.concat(".").concat(innerValueTag));

    }

    /**
     * Checks if a tag exists
     *
     *
     * @param tag
     * @return
     */
    public static boolean sortingTagExists(String tag) {
        return sortingTags.get(tag) != null;
    }

    public static boolean filteringTagExists(String tag) {
        return filteringTags.get(tag) != null;
    }

    public static String getFilteringTagValue(String tag) {
        return filteringTags.get(tag);
    }
}
