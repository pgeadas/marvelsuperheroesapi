package com.pgeadas.notes.constants;

import java.util.HashMap;

/**
 *
 * @author geadas
 */
public final class ServiceNotesTags implements ServiceCommons {

    //note tag
    public static final String notesTag = "notes";
    public static final String notesCountTag = "notesCount";

    //available tags for filtering and sorting of Notes
    public static final String creationDateTag = "creationDate";
    public static final String modificationDateTag = "modificationDate";
    public static final String creatorIdTag = "creatorId";
    public static final String creatorNameContainsTag = "creatorNameContains";
    public static final String creatorNameEqualsTag = "creatorNameEquals";
    public static final String descriptionContainsTag = "descriptionContains";
    public static final String descriptionEqualsTag = "descriptionEquals";
    public static final String hashTag = "hash";

    //no need to add this to the map
    public static final String hrefTag = "href";
    //description field in database (not for query purposes, so no need to add to hash)
    public static final String descriptionTag = "description";
    //creatorName field in database (not for query purposes, so no need to add to hash)
    public static final String creatorNameTag = "creatorName";

    private static final HashMap<String, String> filteringTags = new HashMap<String, String>();

    public ServiceNotesTags() {
        filteringTags.put(creationDateTag, "");
        filteringTags.put(modificationDateTag, "");
        filteringTags.put(creatorIdTag, "");
        filteringTags.put(creatorNameContainsTag, "");
        filteringTags.put(creatorNameEqualsTag, "");
        filteringTags.put(descriptionContainsTag, "");
        filteringTags.put(descriptionEqualsTag, "");
        filteringTags.put(limitTag, "");
        filteringTags.put(offsetTag, "");
        filteringTags.put(hashTag, "");
    }

    /**
     * Checks if a tag exists
     *
     * @param tag
     * @return
     */
    public static boolean filteringTagExists(String tag) {
        return filteringTags.get(tag) != null;
    }
}
