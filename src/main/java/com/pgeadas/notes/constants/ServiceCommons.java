/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pgeadas.notes.constants;

/**
 *
 * @author geadas
 */
public interface ServiceCommons {

    public static final String limitTag = "limit";
    public static final String offsetTag = "offset";
    public static final String countTag = "count";
    public static final String totalCountTag = "totalCount";
    public static final String responseInfoTag = "response info";
    public static final String responseDataTag = "response data";
    public static final String statusTag = "status";
    public static final String statusMsgTag = "status message";
    public static final String nextTag = "next";
    public static final String previousTag = "previous";
    public static final String firstTag = "first";
    public static final String lastTag = "last";
    
    //another constants used in all project
    // Properties, log, etc
    public final static String ENCODING = "UTF-8";
    public final static String NOTES_PROVIDER_PROPERTIES_FILENAME = "config.properties";
    //used to store the Marvel API keys
    public final static String MARVEL_API_KEYS_FILENAME = "keys.properties";
    public final static String PUBLIC_KEY_TAG = "marvel.api.keys.public";
    public final static String PRIVATE_KEY_TAG = "marvel.api.keys.private";
    public final static String MARVEL_API_URI = "https://gateway.marvel.com:443/v1/public/creators";
    //used to get the shared resources
    public final static String MARVEL_API_COMICS_URI = "https://gateway.marvel.com:443/v1/public/comics";
    public final static String MARVEL_API_SERIES_URI = "https://gateway.marvel.com:443/v1/public/series";
    public final static String MARVEL_API_STORIES_URI = "https://gateway.marvel.com:443/v1/public/stories";
    public final static String MARVEL_API_EVENTS_URI = "https://gateway.marvel.com:443/v1/public/events";
    //schemas to validate the requests
    public final static String POST_SCHEMA_FILENAME = "POST_SCHEMA.json";
    public final static String PUT_SCHEMA_FILENAME = "PUT_SCHEMA.json";
    public final static String DELETE_SCHEMA_FILENAME = "DELETE_SCHEMA.json";
    //constants to use in queries to database
    public final static int DEFAULT_LIMIT = 5;
    public final static int MAX_LIMIT = 100;
    public final static int DEFAULT_OFFSET = 0;

}
