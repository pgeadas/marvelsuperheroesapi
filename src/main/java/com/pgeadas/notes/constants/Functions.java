package com.pgeadas.notes.constants;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import javax.ws.rs.core.UriInfo;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author geadas
 *
 * * Util class with some common functions used throughout the project
 *
 */
public final class Functions {

    /**
     * Checks if a custom limit and offset are in the request. If not, sets this
     * properties with the custom values.
     *
     * @param limitKey
     * @param offsetKey
     * @param uriInfo
     * @return
     */
    public static Pair checkLimitAndOffset(String limitKey, String offsetKey, UriInfo uriInfo) {
        int limit, offset;
        if (uriInfo.getQueryParameters().get(limitKey) == null) {
            limit = ServiceCommons.DEFAULT_LIMIT;
        } else {
            limit = Integer.parseInt(uriInfo.getQueryParameters().get(limitKey).get(0));
            if (limit > ServiceCommons.MAX_LIMIT) {
                throw new RuntimeException("Limit cannot exceed the maximum allowed: " + ServiceCommons.MAX_LIMIT);
            }
        }
        if (uriInfo.getQueryParameters().get(offsetKey) == null) {
            offset = ServiceCommons.DEFAULT_OFFSET;
        } else {
            offset = Integer.parseInt(uriInfo.getQueryParameters().get(offsetKey).get(0));
        }

        return Pair.of(limit, offset);
    }

    /**
     * Creates an MD5 digest from text
     *
     *
     * @param text
     * @return
     * @throws java.security.NoSuchAlgorithmException
     */
    public static String createMD5Digest(String text) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.reset();
        m.update(text.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        String hashtext = bigInt.toString(16);
        // Now we need to zero pad it to get the full 32 chars.
        while (hashtext.length() < 32) {
            hashtext = "0" + hashtext;
        }

        return hashtext;
    }

    /**
     * Read a properties file by path. Used when the file is within the
     * resources folder but needs to be reloaded every time we run the class,
     * with no need to re-compile(to check for changes)
     *
     *
     * @param path
     * @param encoding
     * @return
     * @throws java.io.FileNotFoundException
     */
    public static Properties getProperties(String path, String encoding) throws FileNotFoundException, IOException {
        Properties properties = null;
        try {
            InputStream inputStream = new FileInputStream(path);
            BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream, encoding));
            properties = new Properties();
            properties.load(buffer);
        } catch (FileNotFoundException ex) {
            throw ex;
        } catch (IOException ex) {
            throw ex;
        }
        return properties;
    }

    /**
     * Reads a Properties file from resources
     *
     * @param fileName
     * @return
     * @throws java.io.IOException
     */
    public static Properties getPropertiesResource(String fileName) throws IOException {
        // loading resource using getResourceAsStream() method 
        InputStream in = Functions.class.getClassLoader().getResourceAsStream(fileName);
        Properties config = new Properties();
        try {
            config.load(in);
        } catch (IOException ex) {
            throw ex;
        }
        return config;
    }

    /**
     * Reads a Properties file from resources as a string
     *
     *
     * @param resourceName
     * @param encoding
     * @return
     * @throws java.io.IOException
     */
    public static String getResourceAsStream(String resourceName, String encoding) throws IOException {
        // loading resource using getResourceAsStream() method 
        StringBuilder result = new StringBuilder();
        InputStream inputStream;
        InputStreamReader streamReader;
        BufferedReader br;

        try {
            inputStream = Functions.class.getClassLoader().getResourceAsStream(resourceName);
            streamReader = new InputStreamReader(inputStream, encoding);
            br = new BufferedReader(streamReader);
            try {
                String line;
                while ((line = br.readLine()) != null) {
                    result.append(line);
                }
            } catch (IOException ex) {
                throw ex;
            } finally {
                inputStream.close();
                streamReader.close();
                br.close();
            }
        } catch (IOException ex) {
            throw ex;
        }
        return result.toString();
    }

    /**
     * Reads a file from system and returns it as a String.
     *
     *
     * @param path
     * @param encoding
     * @return
     * @throws java.io.FileNotFoundException
     */
    public static String readFile(String path, String encoding) throws FileNotFoundException, IOException {
        StringBuilder content = new StringBuilder();
        InputStream inputStream;
        InputStreamReader streamReader;
        BufferedReader br;
        try {
            inputStream = new FileInputStream(path);
            streamReader = new InputStreamReader(inputStream, encoding);
            br = new BufferedReader(streamReader);
            try {
                String line;
                while ((line = br.readLine()) != null) {
                    content.append(line);
                }
            } catch (IOException ex) {
                throw ex;
            } finally {
                inputStream.close();
                streamReader.close();
                br.close();
            }
        } catch (IOException ex) {
            throw ex;
        }
        return content.toString();
    }

    /**
     * Reads an object from disk.
     *
     *
     * @param path
     * @return
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    public static Object readObjectFromFile(String path) throws IOException, ClassNotFoundException {
        InputStream inputStream;
        ObjectInputStream ois;
        Object content;

        try {
            inputStream = new FileInputStream(path);
            ois = new ObjectInputStream(inputStream);

            try {
                content = ois.readObject();
            } catch (IOException ex) {
                throw ex;
            } finally {
                inputStream.close();
                ois.close();
            }
        } catch (IOException ex) {
            throw ex;
        }
        return content;
    }

    /**
     * Writes an object to disk.
     *
     *
     * @param path
     * @param content
     * @throws java.io.IOException
     */
    public static void writeObjectToFile(String path, Object content) throws IOException {
        FileOutputStream fout;
        ObjectOutputStream oos;
        try {
            fout = new FileOutputStream(path, false);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(content);
            fout.close();
            oos.close();
        } catch (IOException ex) {
            throw ex;
        }

    }

    /**
     * Gets a string from incomingData, which represents the embeded request
     *
     *
     * @param incomingData
     * @return
     * @throws java.io.IOException
     */
    public static String readIncomingData(InputStream incomingData) throws IOException {
        StringBuilder request = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
            String line;
            while ((line = in.readLine()) != null) {
                request.append(line);
            }
        } catch (IOException ex) {
            throw ex;
        }

        return request.toString();
    }

    /**
     * Checks the integrity of a JSON String
     *
     * @param json
     * @return
     */
    public static JSONObject getJSONFromString(String json) {
        return new JSONObject(new JSONTokener(json));
    }

    /**
     * Creates a JSON Object from file in disk
     *
     *
     * @param fileName
     * @param encoding
     * @return
     * @throws java.io.IOException
     */
    public static JSONObject getJsonObjectAsStream(String fileName, String encoding) throws IOException {
        return new JSONObject(getResourceAsStream(fileName, encoding)); //change
    }

    /**
     * Checks the integrity of a JSON String, read from file
     *
     * @param pathToJSON
     * @param encoding
     * @return
     * @throws java.io.IOException, JSONException
     */
    public static JSONObject getJSONObjectFromFile(String pathToJSON, String encoding) throws IOException, JSONException {
        return getJSONFromString(getResourceAsStream(pathToJSON, encoding));
    }

}
