package com.pgeadas.notes.helpers;

import com.pgeadas.notes.exceptions.JsonException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author geadas
 *
 * Util class used to save a String and its JSON representation and a method
 * that allows to convert String to its JSONObject representation and
 * vice-versa. 
 *
 */
public class ResultData {

    private JSONObject object = null;
    private String objectString = "";

    /**
     *
     * @param targetString
     * @param transform
     * @throws JsonException
     */
    public ResultData(String targetString, boolean transform) throws JsonException {
        this.objectString = targetString;
        if (transform) {
            this.object = this.toJSONObject();
        }
    }

    /**
     *
     * @param object
     */
    public ResultData(JSONObject object) {
        this.object = object;
    }

    /**
     *
     * @param targetString
     */
    public ResultData(String targetString) {
        this.objectString = targetString;
    }

    /**
     *
     * @return
     */
    public JSONObject getObject() {
        return object;
    }

    /**
     *
     * @return
     */
    public String getObjectString() {
        if (!objectString.isEmpty()) {
            return objectString;
        } else {
            return object.toString();
        }
    }

    /**
     * Checks the integrity of this JSON String
     *
     * @return
     */
    private JSONObject toJSONObject() throws JsonException {
        return new JSONObject(new JSONTokener(objectString));
    }
}
