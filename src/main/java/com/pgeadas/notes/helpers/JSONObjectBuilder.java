package com.pgeadas.notes.helpers;

import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.constants.ServiceCreatorsTags;
import com.pgeadas.notes.constants.ServiceNotesTags;
import java.util.List;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author geadas
 *
 * Here we define methods that will be used to create JSONObjects that will be
 * sent in the response to the user. The method chosen depends on which
 * information is to be shown.
 */
public interface JSONObjectBuilder {

    /**
     * Builds a JSON array from an array of Not
     *
     * @param objList
     * @return
     */
    public static JSONArray buildFromList(List<Note> objList) {
        JSONArray results = new JSONArray();
        //add the Notes to the response
        if (objList != null) {
            for (Note n : objList) {
                results.put(buildNote(n));
            }
        }
        return results;
    }

    /**
     * @param objList
     * @param uri
     * @param creatorId
     * @return
     */
    public static JSONObject buildFromListByCreatorId(List<Note> objList, String uri, int creatorId) {

        JSONObject finalObject = new JSONObject();
        JSONArray notes = new JSONArray();
        //add the Notes to the response
        if (objList != null) {

            for (Note n : objList) {
                if (n.getCreatorId() == creatorId) {
                    notes.put(buildNoteFilteredByCreatorId(n, uri));
                }
            }

            if (!objList.isEmpty()) {
                finalObject.put(ServiceNotesTags.creatorIdTag, objList.get(0).getCreatorId());
                finalObject.put(ServiceNotesTags.creatorNameTag, objList.get(0).getCreatorName());
            }
        }

        finalObject.put(ServiceNotesTags.notesTag, notes);

        return finalObject;
    }

    /**
     * @param objList
     * @param uri
     * @return
     */
    public static JSONArray buildFromList(List<Note> objList, String uri) {

        JSONArray results = new JSONArray();
        //add the Notes to the response
        if (objList != null) {
            for (Note n : objList) {
                results.put(buildNote(n, uri));
            }
        }

        return results;
    }

    /**
     * Adds the href to the Note JSONObject. This method is used when we are
     * showing the entire note list.
     *
     * @param n
     * @param uri
     * @return
     */
    public static JSONObject buildNote(Note n, String uri) {
        JSONObject note = new JSONObject();
        note.put(ServiceNotesTags.hrefTag, uri + n.getHash());
        note.put(ServiceNotesTags.creationDateTag, n.getCreationDate());
        note.put(ServiceNotesTags.modificationDateTag, n.getModificationDate());
        note.put(ServiceNotesTags.descriptionTag, n.getDescription());
        note.put(ServiceNotesTags.creatorNameTag, n.getCreatorName());
        note.put(ServiceNotesTags.creatorIdTag, n.getCreatorId());
        note.put(ServiceNotesTags.hashTag, n.getHash());
        return note;
    }

    /**
     * Here we dont show the creatorId, since we just filtered the notes by that
     * field.
     *
     * @param n
     * @param uri
     * @return
     */
    public static JSONObject buildNoteFilteredByCreatorId(Note n, String uri) {
        JSONObject note = new JSONObject();
        note.put(ServiceNotesTags.hrefTag, uri + n.getHash());
        note.put(ServiceNotesTags.creationDateTag, n.getCreationDate());
        note.put(ServiceNotesTags.modificationDateTag, n.getModificationDate());
        note.put(ServiceNotesTags.descriptionTag, n.getDescription());
        note.put(ServiceNotesTags.hashTag, n.getHash());
        return note;
    }

    /**
     * No need to show the href, since this is the note we were searching for
     * already
     *
     *
     * @param n
     * @return
     */
    public static JSONObject buildNote(Note n) {
        JSONObject note = new JSONObject();
        note.put(ServiceNotesTags.hashTag, n.getHash());
        note.put(ServiceNotesTags.creationDateTag, n.getCreationDate());
        note.put(ServiceNotesTags.modificationDateTag, n.getModificationDate());
        note.put(ServiceNotesTags.descriptionTag, n.getDescription());
        note.put(ServiceNotesTags.creatorNameTag, n.getCreatorName());
        note.put(ServiceNotesTags.creatorIdTag, n.getCreatorId());
        return note;
    }

    /**
     * No need to show creatorName and creatorId since this is the note we were
     * searching for already and associated with this creator
     *
     * @param n
     * @return
     */
    public static JSONObject buildMinimalNote(Note n) {
        JSONObject note = new JSONObject();
        note.put(ServiceNotesTags.hashTag, n.getHash());
        note.put(ServiceNotesTags.creationDateTag, n.getCreationDate());
        note.put(ServiceNotesTags.modificationDateTag, n.getModificationDate());
        note.put(ServiceNotesTags.descriptionTag, n.getDescription());
        return note;
    }

    /**
     *
     * Adds the custom notes to the Creator object.
     *
     * @param pair
     * @return
     */
    public static JSONObject buildCreatorCustomNotes(Pair<Long, List<Note>> pair) {
        JSONObject finalObj = new JSONObject();
        JSONArray customNotes = new JSONArray();

        for (Note n : pair.getRight()) {
            customNotes.put(JSONObjectBuilder.buildMinimalNote(n));
        }
        finalObj.put(ServiceNotesTags.notesCountTag, pair.getLeft());
        finalObj.put(ServiceNotesTags.notesTag, customNotes);

        return finalObj;
    }

    /**
     *
     * @param count
     * @param notes
     * @return
     */
    public static JSONObject buildCreatorCustomNotes(long count, List<Note> notes) {
        JSONObject finalObj = new JSONObject();
        JSONArray customNotes = new JSONArray();

        for (Note n : notes) {
            customNotes.put(JSONObjectBuilder.buildMinimalNote(n));
        }
        finalObj.put(ServiceNotesTags.notesCountTag, count);
        finalObj.put(ServiceNotesTags.notesTag, customNotes);

        return finalObj;
    }

    /**
     * @param id
     *
     * @param fullName
     * @param modified
     * @param nrStories
     * @param nrComics
     * @param notes
     * @param nrSeries
     * @param nrEvents
     * @return
     */
    public static JSONObject buildCreator(int id, String fullName, String modified, int nrComics, int nrEvents, int nrStories, int nrSeries, JSONObject notes) {
        JSONObject creator = new JSONObject();
        creator.put(ServiceCreatorsTags.idTag, id);
        creator.put(ServiceCreatorsTags.fullNameTag, fullName);
        creator.put(ServiceCreatorsTags.modifiedTag, modified);
        creator.put(ServiceCreatorsTags.comicsTag, nrComics);
        creator.put(ServiceCreatorsTags.eventsTag, nrEvents);
        creator.put(ServiceCreatorsTags.storiesTag, nrStories);
        creator.put(ServiceCreatorsTags.seriesTag, nrSeries);
        creator.put(ServiceCreatorsTags.customNotesTag, notes);
        //log.debug(creator.toString());
        return creator;
    }

    /**
     *
     * @param id
     * @param firstName
     * @param middleName
     * @param lastName
     * @param modified
     * @param nrComics
     * @param nrEvents
     * @param nrStories
     * @param nrSeries
     * @param notes
     * @return
     */
    public static JSONObject buildCreatorForSummary(int id, String firstName, String middleName, String lastName, String modified, int nrComics, int nrEvents, int nrStories, int nrSeries, JSONObject notes) {
        JSONObject creator = new JSONObject();
        creator.put(ServiceCreatorsTags.idTag, id);
        creator.put(ServiceCreatorsTags.firstNameTag, firstName);
        creator.put(ServiceCreatorsTags.middleNameTag, middleName);
        creator.put(ServiceCreatorsTags.lastNameTag, lastName);
        creator.put(ServiceCreatorsTags.modifiedTag, modified);
        creator.put(ServiceCreatorsTags.comicsTag, nrComics);
        creator.put(ServiceCreatorsTags.eventsTag, nrEvents);
        creator.put(ServiceCreatorsTags.storiesTag, nrStories);
        creator.put(ServiceCreatorsTags.seriesTag, nrSeries);
        creator.put(ServiceCreatorsTags.customNotesTag, notes);
        //log.debug(creator.toString());
        return creator;
    }

    /**
     *
     * @param id
     * @param fullName
     * @param modified
     * @param nrComics
     * @param nrEvents
     * @param nrStories
     * @param nrSeries
     * @return
     */
    public static JSONObject buildCreator(int id, String fullName, String modified, int nrComics, int nrEvents, int nrStories, int nrSeries) {
        JSONObject creator = new JSONObject();
        creator.put(ServiceCreatorsTags.idTag, id);
        creator.put(ServiceCreatorsTags.fullNameTag, fullName);
        creator.put(ServiceCreatorsTags.modifiedTag, modified);
        creator.put(ServiceCreatorsTags.comicsTag, nrComics);
        creator.put(ServiceCreatorsTags.eventsTag, nrEvents);
        creator.put(ServiceCreatorsTags.storiesTag, nrStories);
        creator.put(ServiceCreatorsTags.seriesTag, nrSeries);
        return creator;
    }
}
