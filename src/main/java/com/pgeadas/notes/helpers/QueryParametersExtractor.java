package com.pgeadas.notes.helpers;

import com.pgeadas.notes.constants.ServiceNotesTags;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author geadas
 */
public interface QueryParametersExtractor {

    /**
     * Used to get all the parameters in the given URI
     *
     *
     * @param uriInfo
     * @return
     */
    public static String getAllQueryParameters(UriInfo uriInfo) {

        StringBuilder fields = new StringBuilder();
        for (Map.Entry e : uriInfo.getQueryParameters().entrySet()) {
            String key = (String) e.getKey();
            fields.append("&");
            fields.append(key);
            fields.append("=");
            fields.append(uriInfo.getQueryParameters().get(key).get(0));
        }

        return fields.toString();
    }

    /**
     * Returns a string ready to append to the final uri (i.e, with parameters
     * separated by & and =), extracted from the original query.
     *
     *
     * @param queryParameters
     * @return
     */
    public static String getAllQueryParameters(HashMap<String, String> queryParameters) {

        StringBuilder fields = new StringBuilder();
        for (Map.Entry e : queryParameters.entrySet()) {
            String key = (String) e.getKey();
            fields.append("&");
            fields.append(key);
            fields.append("=");
            fields.append(e.getValue());
        }

        return fields.toString();
    }

    /**
     * Returns a string ready to append to the final uri (i.e, with parameters
     * separated by & and =), extracted from the original query, except limit
     * and offset.
     *
     *
     * @param queryParameters
     * @return
     */
    public static String getSomeQueryParameters(HashMap<String, String> queryParameters) {
        //get the parameters from the query, except limit and offset, cause we need to check their new values
        StringBuilder fields = new StringBuilder();
        for (Map.Entry e : queryParameters.entrySet()) {
            String key = (String) e.getKey();
            if (!key.equals(ServiceNotesTags.limitTag) && !key.equals(ServiceNotesTags.offsetTag)) {
                fields.append("&");
                fields.append(key);
                fields.append("=");
                fields.append(e.getValue());
            }
        }
        return fields.toString();
    }

    /**
     * Transforms the MultivaluedMap to HashMap
     *
     *
     * @param queryParameters
     * @return
     */
    public static HashMap<String, String> getHashMapFromMultivaluedMap(MultivaluedMap<String, String> queryParameters) {
        HashMap<String, String> params = new HashMap<>();
        for (Map.Entry e : queryParameters.entrySet()) {
            //log . debug
            params.put((String) e.getKey(), (String) ((List) e.getValue()).get(0));
        }
        return params;
    }
}
