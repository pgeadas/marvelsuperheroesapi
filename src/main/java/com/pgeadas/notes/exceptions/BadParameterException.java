package com.pgeadas.notes.exceptions;

/**
 *
 * @author geadas
 */
public class BadParameterException extends Throwable {

    public BadParameterException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
