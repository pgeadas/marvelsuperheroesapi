package com.pgeadas.notes.exceptions;

/**
 *
 * @author geadas
 */
public class DuplicatesNotAllowedException extends Throwable {

    public DuplicatesNotAllowedException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
