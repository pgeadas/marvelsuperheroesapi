package com.pgeadas.notes.exceptions;

/**
 *
 * @author geadas
 */
public class JsonException extends Throwable {

    private final String myMessage;

    public JsonException(String message) {
        super(message);
        myMessage = message;
    }

    public String getMyMessage() {
        return myMessage;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
