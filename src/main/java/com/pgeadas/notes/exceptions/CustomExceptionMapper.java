package com.pgeadas.notes.exceptions;

import com.jayway.jsonpath.JsonPathException;
import com.pgeadas.notes.response.JSONFaultResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.http.HttpStatus;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Every exception thrown in our App is mapped to this class and we can
 * customise how to respond and inform the user about what happened and how to
 * possibly solve the issue. I like having multiple custom kinds of exceptions
 * cause it allow to distinguish between them and give more refined information
 * than rather the exception message only.*
 */
@Provider
public class CustomExceptionMapper implements ExceptionMapper<Throwable> {

    private static final Logger log = LoggerFactory.getLogger(CustomExceptionMapper.class);

    public CustomExceptionMapper() {
        log.info("CustomExceptionMapper initialised.");
    }

    /**
     * We do log the stack trace of each exception so we know what happened and
     * where the exception was thrown.*
     * @param exception
     * @return 
     */
    @Override
    public Response toResponse(Throwable exception) {
        log.info("Caught exception: Type: " + exception.getClass());
        int status = HttpStatus.SC_OK;
        String addDescription = "", finalResult;
        boolean otherException = false;
        boolean printAndBuildResponse = true;

        try {
            if (exception instanceof JsonPathException || exception instanceof UserDoesNotExistException || exception instanceof BadParameterException) {
                status = HttpStatus.SC_BAD_REQUEST;
                addDescription = exception.getMessage();
            } else if (exception instanceof JsonException || exception instanceof JSONException) {
                status = HttpStatus.SC_BAD_REQUEST;
                addDescription = "Invalid JSON: " + exception.getMessage();
            } else if (exception instanceof ValidationException) {
                status = HttpStatus.SC_BAD_REQUEST;
                addDescription = "Failed schema validation: " + ((ValidationException) exception).getAllMessages().toString();
            } else if (exception instanceof DuplicatesNotAllowedException) {
                //status is still OK, we just add a custom description
                addDescription = exception.getMessage();
            } else if (exception instanceof WebApplicationException || exception instanceof NotFoundException) {
                status = HttpStatus.SC_NOT_FOUND;
                //logs the stack trace, so we know what was the problem, status is definid by the app
                addDescription = exception.getMessage() + ". Either you provided an invalid parameter or the resource you are trying to access does not exist.";
            } else if (exception instanceof HttpCallException) {
                //call to our app was OK, API code is shown in the error msg. Here we return a different kind of response.
                printAndBuildResponse = false;
                addDescription = exception.getMessage();
                log.debug("Exception: '{}';", addDescription, new Throwable(exception));
                finalResult = new JSONFaultResponse().createResponse("Success", new JSONObject(addDescription)).toString();
                return Response.status(status).entity(finalResult).build();
            } else if (exception instanceof IOException || exception instanceof SQLException || exception instanceof ClassCastException || exception instanceof NoSuchAlgorithmException) {
                status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
                addDescription = exception.getMessage();
            } else {
                otherException = true;
            }
        } finally {
            if (printAndBuildResponse) {
                if (!otherException) { //expected exception, we return the custom description
                    log.debug("Exception: '{}';", addDescription, new Throwable(exception));
                    finalResult = new JSONFaultResponse().createResponse(addDescription).toString();
                } else { //other not accounted exception, probably internal server error...
                    status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
                    log.error("Exception: '{}';", addDescription, new Throwable(exception));
                    finalResult = new JSONFaultResponse().createResponse(exception.getMessage()).toString();
                }
            } else { //just builds the response, print was already done
                finalResult = new JSONFaultResponse().createResponse(addDescription).toString();
            }
        }

        log.info("Failed to process request.\nResponse '{}'", finalResult);
        return Response.status(status).entity(finalResult).type(MediaType.APPLICATION_JSON).build();
    }

}
