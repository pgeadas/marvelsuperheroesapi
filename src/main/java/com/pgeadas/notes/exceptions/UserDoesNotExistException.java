package com.pgeadas.notes.exceptions;

/**
 *
 * @author geadas
 */
public class UserDoesNotExistException extends Throwable {

    public UserDoesNotExistException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
