package com.pgeadas.notes.exceptions;

public class HttpCallException extends Throwable {

    private final int statusCode;

    public HttpCallException(String message, int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

}
