package com.pgeadas.notes.resources;

import com.jayway.jsonpath.JsonPathException;
import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.exceptions.BadParameterException;
import com.pgeadas.notes.exceptions.DuplicatesNotAllowedException;
import com.pgeadas.notes.exceptions.HttpCallException;
import com.pgeadas.notes.exceptions.JsonException;
import com.pgeadas.notes.exceptions.UserDoesNotExistException;
import com.pgeadas.notes.helpers.ResultData;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author geadas
 *
 * TODO: check this out to measure performance, seemed nice: http://openjdk.java.net/projects/code-tools/jmh/
 */
@Path("/")
public final class Resources {

    private static final Logger log = LoggerFactory.getLogger(Resources.class);
    private static final RequestProcessor processor = new RequestProcessor();

    /**
     * Checks if the service started successfully.
     *
     * @return
     * @throws com.pgeadas.notes.exceptions.JsonException
     */
    @GET
    @Path("/verify")
    @Produces(MediaType.APPLICATION_JSON)
    public Response doGETverifyService() throws JsonException {
        String result = "{\"Service Status\": \"ONLINE\"}";
        log.info("GET Verify Request received... Returning {}", result);
        return Response.status(HttpStatus.SC_OK).entity(result).type(MediaType.APPLICATION_JSON).build();
    }

    /**
     * Gets the list of creators*
     * @param uriInfo
     * @param username
     */
    @GET
    @Path("{username}/creators/list")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response doGETCreatorsList(@Context UriInfo uriInfo, @PathParam("username") String username) throws JsonException, IOException, HttpCallException, JsonPathException, NoSuchAlgorithmException, SQLException, BadParameterException {
        log.info("GET creators list received {}... Preparing to process...");
        Response response = processor.processGETCreatorsListRequest(username, uriInfo);
        log.info("GET Request Processed Successfully.");
        return response;
    }

    /**
     * Compares two creators*
     * @param uriInfo
     * @param username
     */
    @GET
    @Path("{username}/creators/compare/{Id1}/{Id2}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response doGETCompareCreators(@Context UriInfo uriInfo, @PathParam("username") String username) throws JsonException, IOException, HttpCallException, JsonPathException, NoSuchAlgorithmException, SQLException, BadParameterException {
        log.info("GET creators list received {}... Preparing to process...");
        Pair<Integer, Integer> ids = processor.validateCreatorsPathParameters(uriInfo);
        Response response = processor.processGETCompareCreatorsRequest(ids, username);

        log.info("GET Request Processed Successfully.");
        return response;
    }

    /**
     * Get the notes associated with the creator*
     * @param uriInfo
     * @param username
     * @param limit
     * @param offset
     */
    @GET
    @Path("{username}/notes/")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response doGETNotes(@Context UriInfo uriInfo, @PathParam("username") String username, @QueryParam("limit") @DefaultValue("-1") int limit, @QueryParam("offset") @DefaultValue("-1") int offset) throws JsonException, BadParameterException, JsonPathException, SQLException, ParseException {
        log.info("GET Notes Request received {}... Preparing to process...");
        processor.validateNotesQueryParameters(uriInfo);
        Response response = processor.processGETNotesRequest(username, uriInfo);
        log.info("GET Notes Request Processed Successfully.");
        return response;
    }

    /**
     * Get a note with corresponding hash*
     * @param uriInfo
     * @param hash
     * @param username
     */
    @GET
    @Path("{username}/notes/{hash}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response doGETNote(@Context UriInfo uriInfo, @PathParam("hash") @DefaultValue("") String hash, @PathParam("username") String username) throws JsonException, BadParameterException, JsonPathException, SQLException, ParseException {
        log.info("GET Note Request received hash={}... Preparing to process...", hash);
        processor.validateNotesQueryParameters(uriInfo);
        Response response = processor.processGETNoteRequest(username, hash);
        log.info("GET Note Request Processed Successfully.");
        return response;
    }

    /**
     * Adds a note to the database on the specified creator*
     * @param incomingData
     * @param username
     */
    @POST
    @Path("{username}/notes/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response doPOSTaddNote(InputStream incomingData, @PathParam("username") @DefaultValue("") String username) throws JsonException, IOException, JsonPathException, SQLException, DuplicatesNotAllowedException, UserDoesNotExistException, NoSuchAlgorithmException, HttpCallException, BadParameterException {
        log.info("POST (inputStream) Request received...");
        ResultData postData = processor.processIncomingData(incomingData, true);
        log.info("Got the data from request. Processing...");
        //validate against shcema
        Note b = processor.validatePOSTRequest(postData);
        b.setUsername(username);
        //add the note
        Response response = processor.insertNote(b);
        log.info("POST Request Processed Successfully.");
        return response;
    }

    /**
     * delete method
     *
     * @param incomingData
     * @param username
     */
    @DELETE
    @Path("{username}/notes/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response doDELETENote(InputStream incomingData, @PathParam("username") @DefaultValue("") String username) throws IOException, JsonException, SQLException {
        log.info("DELETE (inputStream) Request received...");
        ResultData putRequestData = processor.processIncomingData(incomingData, true);
        log.info("Got the data. Processing...");
        //validate against schema. 
        Note note = processor.validateDeleteRequest(putRequestData);
        note.setUsername(username);
        //does the deletion
        Response response = processor.deleteNote(note);
        log.info("DELETE Request Processed Successfully.");
        return response;
    }

    /**
     *
     *
     * @param incomingData
     * @param username
     */
    @PUT
    @Path("{username}/notes/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response doPUTupdateNote(InputStream incomingData, @PathParam("username") @DefaultValue("") String username) throws IOException, JsonException, SQLException, JsonPathException, DuplicatesNotAllowedException, UserDoesNotExistException, NoSuchAlgorithmException {
        log.info("UPDATE request received.");
        //read data, if we want it in both String and JSONObject format, we pass boolean indicating
        ResultData putRequestData = processor.processIncomingData(incomingData, true);
        //validates the data received
        Note note = processor.validateUpdateRequest(putRequestData);
        note.setUsername(username);
        //perform the update
        Response response = processor.updateNote(note);
        log.info("UPDATE request processed successfully.");
        return response;
    }
}
