package com.pgeadas.notes.resources;

import com.jayway.jsonpath.JsonPathException;
import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.constants.ServiceCommons;
import com.pgeadas.notes.constants.Functions;
import com.pgeadas.notes.constants.ServiceCreatorsTags;
import com.pgeadas.notes.constants.ServiceNotesTags;
import com.pgeadas.notes.database.strategy.DatabaseContext;
import com.pgeadas.notes.database.postgresql.connectors.HerokuPostgresqlConnector;
import com.pgeadas.notes.database.postgresql.connectors.LocalPostgresqlConnector;
import com.pgeadas.notes.exceptions.BadParameterException;
import com.pgeadas.notes.exceptions.DuplicatesNotAllowedException;
import com.pgeadas.notes.exceptions.HttpCallException;
import com.pgeadas.notes.exceptions.JsonException;
import com.pgeadas.notes.exceptions.UserDoesNotExistException;
import com.pgeadas.notes.marvelapi.JsonPathReader;
import com.pgeadas.notes.marvelapi.MarvelAPICaller;
import com.pgeadas.notes.response.JSONFaultResponse;
import com.pgeadas.notes.response.JSONResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpStatus;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebListener
public final class Provider implements ServletContextListener {

    private final static Logger log = LoggerFactory.getLogger(Provider.class);
    //Current database used is now selectable through a Strategy pattern
    private static DatabaseContext dbContext;
    private static MarvelAPICaller marvelApiCaller;
    public static Schema UPDATE_SCHEMA_VALIDATOR;
    public static Schema POST_SCHEMA_VALIDATOR;
    public static Schema DELETE_SCHEMA_VALIDATOR;
    private static Properties props;
    private static Properties theKeys;
    //TODO: private static APICache CREATORS_CACHE;
    //default return code
    private static final int subStatus = HttpStatus.SC_OK;

    //load and initialisation of resources is now done at startup, instead of when the first request arrives
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            log.info("Notes Provider startup init...");
            loadProperties();
            //here we could load them from properties as well
            initServiceTags();
            initDatabaseContext();
            //initCreatorsCache();
            initMarvelAPICaller();
            deleteProperties();
            readSchemas();
            log.info("Notes Provider startup completed.");
        } catch (Throwable ex) {
            log.error("Critical Exception Provider Exception caught: : '{}' '{}'", ex.getMessage(), ex);
            log.error("Shutting down...");
            System.exit(1);
        }
    }

    /**
     * Populates our Maps with available tags to getNotes and Creators so we can
     * use them from everywhere. Since they are static, we do not need to store
     * an Instance of the class.*
     */
    private void initServiceTags() {
        new ServiceNotesTags();
        new ServiceCreatorsTags();
    }

    private void loadProperties() throws IOException {
        log.info("Loading properties files '{}', '{}'...", ServiceCommons.NOTES_PROVIDER_PROPERTIES_FILENAME, ServiceCommons.MARVEL_API_KEYS_FILENAME);
        try {
            props = Functions.getPropertiesResource(ServiceCommons.NOTES_PROVIDER_PROPERTIES_FILENAME);
            theKeys = Functions.getPropertiesResource(ServiceCommons.MARVEL_API_KEYS_FILENAME);
        } catch (IOException ex) {
            String description = "Failed to load properties file from resources! {}" + ex.getMessage();
            log.error(description);
            throw new IOException(description);
        }
        log.info("Properties files loaded successfully.");
    }

    /**
     * Delete the reference to properties file, so it is marked to be garbage
     * collected and removed from memory as soon as it is not needed.*
     */
    private void deleteProperties() {
        props = null;
    }

    /**
     * If some cleanup needs to be performed when App exits, will be done here.
     *
     * @param sce
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        dbContext.getActualConnector().shutdown();
    }

    /**
     * Reads the schemas used to validate requests from database*
     */
    private void readSchemas() throws IOException, JSONException {
        log.debug("Loading schemas from resources: '{}', '{}', '{}'",
                ServiceCommons.PUT_SCHEMA_FILENAME, ServiceCommons.DELETE_SCHEMA_FILENAME, ServiceCommons.POST_SCHEMA_FILENAME);
        readPOSTschema();
        readPUTschema();
        readDELETEschema();
        log.debug("Loading schemas from resources completed.");
    }

    private void readPOSTschema() throws IOException, JSONException {
        log.debug("POST Schema will load...");
        POST_SCHEMA_VALIDATOR = SchemaLoader.load(Functions.getJSONObjectFromFile(ServiceCommons.POST_SCHEMA_FILENAME, ServiceCommons.ENCODING));
        log.debug("POST Schema loaded successfully.");
    }

    private void readPUTschema() throws IOException, JSONException {
        log.debug("PUT Schema will load...");
        UPDATE_SCHEMA_VALIDATOR = SchemaLoader.load(Functions.getJSONObjectFromFile(ServiceCommons.PUT_SCHEMA_FILENAME, ServiceCommons.ENCODING));
        log.debug("PUT Schema loaded successfully.");
    }

    private void readDELETEschema() throws IOException, JSONException {
        log.debug("DELETE Schema will load...");
        DELETE_SCHEMA_VALIDATOR = SchemaLoader.load(Functions.getJSONObjectFromFile(ServiceCommons.DELETE_SCHEMA_FILENAME, ServiceCommons.ENCODING));
        log.debug("DELETE Schema loaded successfully.");
    }

    /**
     * Sets the initial DB Context in use, SQL or another DB that may be added
     * later..
     */
    private void initDatabaseContext() throws IOException, URISyntaxException {
        log.info("Database Context initialising...");
        //1-SQL
        switch (props.getProperty("database.context").toLowerCase()) {
            case "localpostgresql":
                String envCtxLookup = props.getProperty("database.envCtx.lookup");
                if (envCtxLookup.isEmpty()) {
                    log.error("Database config missing: ctxLookup and envCtxLookup must be not null! Application cannot continue. See config.properties for options.");
                    System.exit(1);
                } else {
                    dbContext = new DatabaseContext(new LocalPostgresqlConnector(envCtxLookup));
                }
                break;
            case "herokupostgresql":
                dbContext = new DatabaseContext(new HerokuPostgresqlConnector());
                break;
            default:
                log.error("No Database Context defined! Application cannot continue. See config.properties for options.");
                System.exit(1);
        }
        log.info("Database context '{}' initialised successfully.", dbContext.getActualConnector().getClass().toString());
    }

    private void initMarvelAPICaller() throws IOException, JsonException {
        log.info("Marvel API Caller initialising...");
        try {
            if (props.getProperty("parser.context").toLowerCase().equals("jsonpath")) {
                marvelApiCaller = new MarvelAPICaller(new JsonPathReader());
            } else {
                log.error("No Parser Context defined! Application cannot continue. See config.properties for options.");
                System.exit(1);
            }
        } catch (IOException ex) {
            String description = "Failed to init Marvel API Caller! {}" + ex.getMessage();
            log.error(description);
            throw new IOException(description);
        }

        log.info("Marvel API Caller '{}' initialised successfully.", marvelApiCaller.getActualParserType());
    }

    /**
     * Validates a request against the respective schema
     *
     *
     * @param requestString
     * @param validator
     * @return
     */
    public boolean validateWithSchema(String requestString, Schema validator) throws JSONException {
        return validateJSONRequest(requestString, validator);
    }

    /**
     * Validates request against the schema. Throws SchemaValidationException if
     * an error occurs. If an error occurs, ExceptionMapper will catch it and
     * tell the user about the problems he needs to fix.
     *
     * @param requestString
     * @param validator
     * @return
     */
    private boolean validateJSONRequest(String requestJSON, Schema validator) throws JSONException {
        validator.validate(Functions.getJSONFromString(requestJSON)); // throws a ValidationException if this object is invalid
        return true;
    }

    public Response insertNote(Note note) throws JsonPathException, SQLException, JsonException, DuplicatesNotAllowedException, UserDoesNotExistException, NoSuchAlgorithmException, IOException, BadParameterException, HttpCallException {
        JSONObject response;
        //get timestamp
        long ts = Instant.now().getEpochSecond();
        String creatorName = Provider.marvelApiCaller.getCreatorName(ServiceCommons.MARVEL_API_URI, note.getCreatorId(), ts, theKeys.getProperty(ServiceCommons.PRIVATE_KEY_TAG), theKeys.getProperty(ServiceCommons.PUBLIC_KEY_TAG));
        note.setCreatorName(creatorName);
        dbContext.insertNote(note);
        response = new JSONResponse().createResponse("Success", "Note inserted.");
        return Response.status(subStatus)
                .entity(response.toString()).type(MediaType.APPLICATION_JSON).build();
    }

    public Response deleteNote(Note note) throws JsonPathException, SQLException, JsonException {
        JSONObject response;
        if (dbContext.deleteNote(note) < 1) {
            log.info("Note not found.");
            response = new JSONFaultResponse().createResponse("Note not found.");
            log.debug("Response: " + response.toString());
        } else {
            response = new JSONResponse().createResponse("Success", "Note deleted.");
        }
        return Response.status(subStatus)
                .entity(response.toString()).type(MediaType.APPLICATION_JSON).build();
    }

    public Response updateNote(Note note) throws JsonPathException, SQLException, JsonException {
        JSONObject response;
        if (dbContext.updateNote(note) < 1) {
            log.info("Note not found.");
            response = new JSONFaultResponse().createResponse("Note not found.");
            log.debug("Response: " + response.toString());
        } else {
            response = new JSONResponse().createResponse("Success", "Note updated.");
        }
        return Response.status(subStatus)
                .entity(response.toString()).type(MediaType.APPLICATION_JSON).build();
    }

    protected Response getNotes(String username, Pair<Integer, Integer> limitAndOffset, String uri, HashMap<String, String> queryParameters) throws ParseException, SQLException, BadParameterException {
        JSONObject response;
        Pair<Long, List<Note>> countAndNotes = dbContext.getNoteS(username, limitAndOffset, Optional.of(queryParameters), Optional.empty());
        log.debug("{} notes found in database.", countAndNotes.getLeft());
        response = new JSONResponse().createResponse(countAndNotes.getLeft(), countAndNotes.getRight(), countAndNotes.getRight().size(), limitAndOffset, uri, queryParameters);
        return Response.status(subStatus)
                .entity(response.toString()).type(MediaType.APPLICATION_JSON).build();
    }

    protected Response getNote(Note note) throws ParseException, SQLException {
        JSONObject response;
        List<Note> results = dbContext.getNote(note);
        log.debug("{} notes found in database.", results.size());
        response = new JSONResponse().createResponse(results);
        return Response.status(subStatus)
                .entity(response.toString()).type(MediaType.APPLICATION_JSON).build();
    }

    protected Response getCompareCreators(Pair<Integer, Integer> ids, String username) throws NoSuchAlgorithmException, IOException, JsonPathException, JsonException, HttpCallException, SQLException, BadParameterException {
        //get timestamp
        long ts = Instant.now().getEpochSecond();
        //call the API
        JSONObject creators = marvelApiCaller.getCompareCreators(ServiceCommons.MARVEL_API_URI, ids.getLeft(), ids.getRight(), dbContext, username, ts, theKeys.getProperty(ServiceCommons.PRIVATE_KEY_TAG), theKeys.getProperty(ServiceCommons.PUBLIC_KEY_TAG));
        String result = new JSONResponse().createMarvelAPIResponse("Success", creators).toString();
        return Response.status(subStatus)
                .entity(result).type(MediaType.APPLICATION_JSON).build();

    }

    protected Response getCreatorsList(String username, HashMap<String, String> allParameters) throws IOException, JsonPathException, JsonException, NoSuchAlgorithmException, SQLException, BadParameterException, HttpCallException {
        //get timestamp
        long ts = Instant.now().getEpochSecond();
        //call the API
        JSONObject creators = marvelApiCaller.getCreatorsList(username, ServiceCommons.MARVEL_API_URI, allParameters, dbContext, ts, theKeys.getProperty(ServiceCommons.PRIVATE_KEY_TAG), theKeys.getProperty(ServiceCommons.PUBLIC_KEY_TAG));
        String result = new JSONResponse().createMarvelAPIResponse("Success", creators).toString();
        return Response.status(subStatus)
                .entity(result).type(MediaType.APPLICATION_JSON).build();
    }

}
