package com.pgeadas.notes.resources;

import com.jayway.jsonpath.JsonPathException;
import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.constants.Functions;
import com.pgeadas.notes.constants.ServiceCreatorsTags;
import com.pgeadas.notes.constants.ServiceNotesTags;
import com.pgeadas.notes.exceptions.BadParameterException;
import com.pgeadas.notes.exceptions.DuplicatesNotAllowedException;
import com.pgeadas.notes.exceptions.HttpCallException;
import com.pgeadas.notes.exceptions.JsonException;
import com.pgeadas.notes.exceptions.UserDoesNotExistException;
import com.pgeadas.notes.helpers.QueryParametersExtractor;
import com.pgeadas.notes.helpers.ResultData;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author geadas
 */
public class RequestProcessor {

    private static final Provider BP = new Provider();
    private static final Logger log = LoggerFactory.getLogger(Resources.class);

    protected Response processGETCompareCreatorsRequest(Pair<Integer, Integer> ids, String username) throws HttpCallException, IOException, JsonPathException, JsonException, NoSuchAlgorithmException, SQLException, BadParameterException {
        log.info("processGETCompareCreatorsRequest...");
        Response response = getCompareCreators(ids, username);
        log.info("processing finished...");
        return response;
    }

    //throw exception to infor the user that soemthing went wrong
    protected Response processGETCreatorsListRequest(String username, UriInfo uriInfo) throws JsonException, IOException, HttpCallException, JsonPathException, NoSuchAlgorithmException, SQLException, BadParameterException {
        log.info("processGETCreatorsListRequest...");
        Response response = getCreatorsList(username, QueryParametersExtractor.getHashMapFromMultivaluedMap(uriInfo.getQueryParameters()));
        log.info("processing finished...");
        return response;
    }

    protected Response processGETNotesRequest(String username, UriInfo uriInfo) throws JsonException, JsonPathException, SQLException, ParseException, BadParameterException {
        log.info("processGETNotesRequest...");
        //checks if we have a custom limit and offset
        Pair<Integer, Integer> limitAndOffset = Functions.checkLimitAndOffset(ServiceNotesTags.limitTag, ServiceNotesTags.offsetTag, uriInfo);
        Response response = getNotes(username, limitAndOffset, uriInfo.getAbsolutePath().toString(), QueryParametersExtractor.getHashMapFromMultivaluedMap(uriInfo.getQueryParameters()));
        log.info("processing finished...");
        return response;
    }

    protected Response processGETNoteRequest(String username, String hash) throws JsonException, JsonPathException, SQLException, ParseException {
        log.info("processGETNoteRequest...");
        Note note = new Note();
        note.setHash(hash);
        note.setUsername(username);
        Response response = getNote(note);
        log.info("processing finished...");
        return response;
    }

    /**
     * Reads the inputStream and stores the data read from it
     *
     *
     * @param incomingData
     * @param format
     * @return
     * @throws java.io.IOException
     * @throws com.pgeadas.notes.exceptions.JsonException
     */
    protected ResultData processIncomingData(InputStream incomingData, boolean format) throws IOException, JsonException {
        log.info("Processing incoming data...");
        String result = Functions.readIncomingData(incomingData);
        log.info("Data Received: '{}'", result);
        return new ResultData(result, format);
    }

    /**
     * Checks if a subscribe request is valid, and creates a bean from the data
     *
     * @param requestData
     * @return
     * @throws com.pgeadas.notes.exceptions.JsonException
     */
    protected Note validateUpdateRequest(ResultData requestData) throws JsonException {
        if (BP.validateWithSchema(requestData.getObjectString(), Provider.UPDATE_SCHEMA_VALIDATOR)) {
            String description = (String) requestData.getObject().get(ServiceNotesTags.descriptionTag);
            String hash = (String) requestData.getObject().get(ServiceNotesTags.hashTag);
            Note note = new Note();
            note.setDescription(description);
            note.setHash(hash);
            return note;
        }
        return null;
    }

    /**
     * Checks the integrity of the received request...
     *
     *
     * @param requestData
     * @return
     * @throws com.pgeadas.notes.exceptions.JsonException
     */
    protected Note validateDeleteRequest(ResultData requestData) throws JsonException {
        if (BP.validateWithSchema(requestData.getObjectString(), Provider.DELETE_SCHEMA_VALIDATOR)) {
            String hash = (String) requestData.getObject().get(ServiceNotesTags.hashTag);
            Note note = new Note(hash);
            return note;
        }
        return null;
    }

    /**
     * Checks the integrity of the received request...
     *
     *
     * @param requestData
     * @return
     * @throws com.pgeadas.notes.exceptions.JsonException
     * @throws com.pgeadas.notes.exceptions.BadParameterException
     */
    protected Note validatePOSTRequest(ResultData requestData) throws JsonException, BadParameterException {
        if (BP.validateWithSchema(requestData.getObjectString(), Provider.POST_SCHEMA_VALIDATOR)) {
            String description = (String) requestData.getObject().get(ServiceNotesTags.descriptionTag);
            int creatorId;
            try {
                creatorId = (int) requestData.getObject().get(ServiceNotesTags.creatorIdTag);
            } catch (ClassCastException ex) {
                throw new BadParameterException("CreatorId is invalid (too big)");
            }
            Note note = new Note(description, creatorId);
            return note;
        }
        return null;
    }

    protected boolean validateNotesQueryParameters(UriInfo uriInfo) throws BadParameterException {
        for (Map.Entry e : uriInfo.getQueryParameters().entrySet()) {
            String key = (String) e.getKey();
            if (!ServiceNotesTags.filteringTagExists(key)) {
                throw new BadParameterException(String.format("Query Parameter '%s' is not recognised.", key));
            }
        }
        return true;
    }

    protected Pair<Integer, Integer> validateCreatorsPathParameters(UriInfo uriInfo) throws BadParameterException {
        int id1, id2;
        try {
            id1 = Integer.parseInt(uriInfo.getPathParameters().getFirst(ServiceCreatorsTags.creatorId1Tag));
        } catch (NumberFormatException ex) {
            throw new BadParameterException(String.format("Sorry, '%s' is an invalid Id!", uriInfo.getPathParameters().getFirst(ServiceCreatorsTags.creatorId1Tag)));
        }

        try {
            id2 = Integer.parseInt(uriInfo.getPathParameters().getFirst(ServiceCreatorsTags.creatorId2Tag));
        } catch (NumberFormatException ex) {
            throw new BadParameterException(String.format("Sorry, '%s' is an invalid Id!", uriInfo.getPathParameters().getFirst(ServiceCreatorsTags.creatorId2Tag)));
        }

        return Pair.of(id1, id2);
    }

    protected Response deleteNote(Note note) throws JsonPathException, SQLException, JsonException {
        return BP.deleteNote(note);
    }

    protected Response insertNote(Note note) throws JsonPathException, SQLException, JsonException, DuplicatesNotAllowedException, UserDoesNotExistException, NoSuchAlgorithmException, HttpCallException, BadParameterException, IOException {
        return BP.insertNote(note);
    }

    protected Response updateNote(Note note) throws JsonPathException, SQLException, JsonException, DuplicatesNotAllowedException, UserDoesNotExistException, NoSuchAlgorithmException {
        return BP.updateNote(note);
    }

    protected Response getNotes(String username, Pair limitAndOffset, String uri, HashMap<String, String> queryParameters) throws JsonPathException, SQLException, JsonException, ParseException, BadParameterException {
        return BP.getNotes(username, limitAndOffset, uri, queryParameters);
    }

    protected Response getNote(Note note) throws JsonPathException, SQLException, JsonException, ParseException {
        return BP.getNote(note);
    }

    protected Response getCreatorsList(String username, HashMap<String, String> allParameters) throws HttpCallException, IOException, JsonPathException, JsonException, NoSuchAlgorithmException, SQLException, BadParameterException {
        return BP.getCreatorsList(username, allParameters);
    }

    protected Response getCompareCreators(Pair<Integer, Integer> ids, String username) throws HttpCallException, IOException, JsonPathException, JsonException, NoSuchAlgorithmException, SQLException, BadParameterException {
        return BP.getCompareCreators(ids, username);
    }

}
