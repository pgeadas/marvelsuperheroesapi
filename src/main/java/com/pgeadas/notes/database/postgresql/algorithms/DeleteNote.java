package com.pgeadas.notes.database.postgresql.algorithms;

import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.database.strategy.ResultsStrategy;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author geadas
 */
public class DeleteNote implements ResultsStrategy {

    private final StringBuilder query
            = new StringBuilder("delete from notes n using users u, user_notes un where "
                    + "un.user_id = u.id and un.note_id = n.id and u.name LIKE '");

    public DeleteNote(Note note) throws SQLException {
        buildDeleteNoteQuery(note);
    }

    @Override
    public Object getResultsFromQuery(ResultSet rs) throws SQLException {
        long count = -1;
        log.debug("Getting results...");
        if (rs.next()) {
            count = rs.getInt(1);
        }
        log.debug("{} - Records deleted: {}", this.getClass(), count);
        return count;
    }

    /**
     * Builds the query for deleting notes. Returns 1 if the update was
     * successful, 0 otherwise.
     *
     * @param note
     * @throws java.sql.SQLException
     */
    private void buildDeleteNoteQuery(Note note) throws SQLException {
        query.append(note.getUsername());
        query.append("' and n.hash LIKE '");
        query.append(note.getHash());
        query.append("' is TRUE RETURNING 1;");
    }

    @Override
    public String getQuery() {
        return this.query.toString();
    }

}
