package com.pgeadas.notes.database.postgresql.algorithms;

import com.pgeadas.notes.database.strategy.ResultsStrategy;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author geadas
 */
public class GetCount implements ResultsStrategy {

    private final StringBuilder query = 
            new StringBuilder("select count(*) from notes n, users u, user_notes un ") ;

    public GetCount(String whereClause) throws SQLException {
        buildCountNotesQuery(whereClause);
    }

    @Override
    public Object getResultsFromQuery(ResultSet rs) throws SQLException {
        long count = -1;
        if (rs.next()) {
            count = rs.getLong("count");
        }
        log.debug("{} - Records counted: {}", this.getClass(),count);
        return count;
    }

    private void buildCountNotesQuery(String whereClause) throws SQLException {
        query.append(whereClause);
    }

    @Override
    public String getQuery()  {
        return this.query.toString();
    }

}
