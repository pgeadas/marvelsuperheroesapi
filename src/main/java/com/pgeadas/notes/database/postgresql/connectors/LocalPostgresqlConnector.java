package com.pgeadas.notes.database.postgresql.connectors;

import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.database.strategy.ConnectorTypeStrategy;
import com.pgeadas.notes.database.strategy.ResultsStrategy;
import com.pgeadas.notes.exceptions.BadParameterException;
import com.pgeadas.notes.exceptions.DuplicatesNotAllowedException;
import com.pgeadas.notes.exceptions.UserDoesNotExistException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author geadas
 *
 * !!!TODO!!!: I just realised that totally forgot about sanitasing the input of
 * the user for strings, so it would be really URGENT thing to correct to avoid
 * SQL Injection problems if the application was going to production.
 *
 * (There are really some improvements that can be done with the database
 * prepared statements, transactions, indexing, a cache... however for this test
 * challenge I really could not spend more time with this optional requirement
 * and I did spent a good chunk of time with it. Using a simple List of beans
 * and write to disk would have taken 5% of the time. Of course that will not be
 * used in a real scenario and this one could.)
 */
public final class LocalPostgresqlConnector extends ConnectorTypeStrategy {

    private DataSource connectionPool;
    private final String ctxLookup = "java:comp/env";
    private final String envCtxLookup;
    private final PostgresqlQueryMaker sqlqm = new PostgresqlQueryMaker(this);

    public LocalPostgresqlConnector(String envCtxLookup) {
        log.info("Initiating database connection.... ");
        this.envCtxLookup = envCtxLookup;
        init();
        log.info("Database connection established successfully. ");
    }

    @Override
    protected void init() {
        try {
            Context ctx = new InitialContext();
            if (ctx == null) {
                throw new RuntimeException("RuntimeException - No Context");
            }
            Context envCtx = (Context) ctx.lookup(ctxLookup);
            //initiates the connection pool to the database, closing connections actually only return them to the pool
            connectionPool = (DataSource) envCtx.lookup(envCtxLookup);
            if (connectionPool != null) {
                log.info("DataSource located successfully.");
            }
        } catch (Exception e) {
            log.error("Something went wrong with the database, {}", e.getMessage());
        }
    }

    @Override
    public void shutdown() {
        log.info("Closing database connection pool.... ");
        connectionPool = null;
        log.info("Closed connection to database.");
    }

    @Override
    public Object makeQuery(String query, ResultsStrategy rStrategy) throws SQLException {
        return sqlqm.makeQuery(query, rStrategy, connectionPool.getConnection());
    }

    @Override
    public long insertNote(Note note) throws SQLException, UserDoesNotExistException, NoSuchAlgorithmException, DuplicatesNotAllowedException {
        return sqlqm.insertNote(note);
    }

    @Override
    public long deleteNote(Note note) throws SQLException {
        return sqlqm.deleteNote(note);
    }

    @Override
    public long updateNote(Note note) throws SQLException {
        return sqlqm.updateNote(note);
    }

    @Override
    public List getNote(Note note) throws SQLException {
        return sqlqm.getNote(note);
    }

    @Override
    public Pair<Long, List<Note>> getNotes(String name, Pair limitAndOffset, Optional<HashMap<String, String>> queryParameters, Optional<Integer> creatorId) throws SQLException, BadParameterException {
        return sqlqm.getNotes(name, limitAndOffset, queryParameters, creatorId);
    }
}
