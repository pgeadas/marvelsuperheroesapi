package com.pgeadas.notes.database.postgresql.connectors;

import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.database.postgresql.algorithms.DeleteNote;
import com.pgeadas.notes.database.postgresql.algorithms.GetCount;
import com.pgeadas.notes.database.postgresql.algorithms.GetNote;
import com.pgeadas.notes.database.postgresql.algorithms.GetNotes;
import com.pgeadas.notes.database.postgresql.algorithms.InsertNote;
import com.pgeadas.notes.database.postgresql.algorithms.UpdateNote;
import com.pgeadas.notes.database.strategy.ConnectorTypeStrategy;
import com.pgeadas.notes.database.strategy.ResultsStrategy;
import com.pgeadas.notes.exceptions.BadParameterException;
import com.pgeadas.notes.exceptions.DuplicatesNotAllowedException;
import com.pgeadas.notes.exceptions.UserDoesNotExistException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Since there are two connectors who use these same methods (in fact they only
 * differ in the type of dataSource they use basically) we separate the concrete
 * implementation of the algorithm operations on the database and abstract their
 * calls in both SQLConnectors inside the functions they must implement from the
 * interface they implement (ConnectorTypeStrategy). This way we only have to
 * change code in this class, which makes the code easier to maintain, cleaner
 * and avoid code repetition, while keeping the ability of changing the
 * connector type easily which was previously implemented (Strategy pattern).
 *
 * @author geadas
 */
public final class PostgresqlQueryMaker {

    private final static Logger log = LoggerFactory.getLogger(PostgresqlQueryMaker.class);
    private final ConnectorTypeStrategy connector;

    public PostgresqlQueryMaker(ConnectorTypeStrategy connector) {
        this.connector = connector;
    }

    public Object makeQuery(String query, ResultsStrategy rStrategy, Connection conn) throws SQLException {
        log.debug("Making query to database...");
        Statement stmt = null;  // TODO: PreparedStatement if possible
        ResultSet rs = null;
        Object results = new Object();
        log.info("Making query to DB...");
        try {
            // conn = ... connection from connection pool from the current connector
            // log.debug("Creating statement...");
            stmt = conn.createStatement();
            //log.debug("Executing query... {}", query);
            rs = stmt.executeQuery(query);
            // log.debug("Getting results...");
            results = rStrategy.getResultsFromQuery(rs);
            // log.debug("Closing connection...");
            rs.close();
            rs = null;
            stmt.close();
            stmt = null;
            conn.close(); // Return to connection pool
            conn = null;  // Make sure we don't close it twice
        } catch (SQLException ex) {
            // ... deal with errors ...
            log.error("Error retrieving data from database: {}", ex.getMessage());
        } finally {
            // Always make sure result sets and statements are closed,
            // and the connection is returned to the pool
            closeResources(rs, stmt, conn);
        }
        log.debug("Returning results from query...");
        return results;
    }

    protected long insertNote(Note note) throws SQLException, UserDoesNotExistException, NoSuchAlgorithmException, DuplicatesNotAllowedException {
        InsertNote in = new InsertNote();

        long userId = (long) connector.makeQuery(in.buildCheckUserExistenceQuery(note.getUsername()), in);

        if (userId == -1) {
            throw new UserDoesNotExistException("Failed to insert Note: User does not exist");
        }

        if ((long) connector.makeQuery(in.buildCheckInsertNoteDuplicateQuery(note), in) == -1) {
            throw new DuplicatesNotAllowedException("Failed to insert Note: Duplicates not allowed");
        }

        //build the query
        in.buildInsertNoteQuery(userId);

        return (long) connector.makeQuery(in.getQuery(), in);
    }

    protected long deleteNote(Note note) throws SQLException {
        DeleteNote dn = new DeleteNote(note);
        return (long) connector.makeQuery(dn.getQuery(), dn);
    }

    protected long updateNote(Note note) throws SQLException {
        UpdateNote un = new UpdateNote(note);
        return (long) connector.makeQuery(un.getQuery(), un);
    }

    protected List getNote(Note note) throws SQLException {
        GetNote gn = new GetNote(note);
        return (List) connector.makeQuery(gn.getQuery(), gn);
    }

    protected Pair<Long, List<Note>> getNotes(String name, Pair limitAndOffset, Optional<HashMap<String, String>> queryParameters, Optional<Integer> creatorId) throws SQLException, BadParameterException {
        GetNotes gn = new GetNotes();
        GetCount gc = new GetCount(gn.getWhereClause(name, limitAndOffset, queryParameters, creatorId));

        long count = (long) connector.makeQuery(gc.getQuery(), gc);

        //get the count of the notes and if there are notes, get them
        if (count == 0) {
            return Pair.of(count, new LinkedList());
        } else {
            return Pair.of(count, (List) connector.makeQuery(gn.getQuery(), gn));
        }
    }

    protected void closeResources(ResultSet rs, Statement stmt, Connection conn) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {;
            }
            rs = null;
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {;
            }
            stmt = null;
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {;
            }
            conn = null;
        }
    }

}
