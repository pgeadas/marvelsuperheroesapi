package com.pgeadas.notes.database.postgresql.algorithms;

import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.constants.Functions;
import com.pgeadas.notes.database.strategy.ResultsStrategy;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author geadas
 *
 *
 */
public class InsertNote implements ResultsStrategy {

    private String hash;
    private final StringBuilder query;

    public InsertNote() {
        query = new StringBuilder();
    }

    @Override
    public Object getResultsFromQuery(ResultSet rs) throws SQLException {
        long count = -1;
        log.debug("Trying to get results from ResultSet...");
        if (rs.next()) {
            count = rs.getInt(1);
        }
        log.debug("{} - Records processed: {}", this.getClass(), count);
        return count;
    }

    public String buildCheckInsertNoteDuplicateQuery(Note note) throws NoSuchAlgorithmException {
        StringBuilder builder = new StringBuilder();
        StringBuilder toGenerateHash = new StringBuilder();
        toGenerateHash.append(note.getUsername());
        toGenerateHash.append(note.getDescription());
        toGenerateHash.append(note.getCreatorId());
        hash = Functions.createMD5Digest(toGenerateHash.toString());
        builder.append("insert into notes(values(nextval('notes_serial'), '");
        builder.append(hash);
        builder.append("', (SELECT LOCALTIMESTAMP(0)), (SELECT LOCALTIMESTAMP(0)), '");
        builder.append(note.getDescription());
        builder.append("', '");
        builder.append(note.getCreatorName());
        builder.append("', ");
        builder.append(note.getCreatorId());
        builder.append(")) RETURNING 1");
        return builder.toString();
    }

    public void buildInsertNoteQuery(long userId) {
        query.append("insert into user_notes values(");
        query.append(userId);
        query.append(", (SELECT n.id from notes n where n.hash ILIKE '");
        query.append(hash);
        query.append("')) RETURNING 1;");
    }

    public String buildCheckUserExistenceQuery(String username) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT u.id from users u where u.name ILIKE '");
        builder.append(username);
        builder.append("'");
        return builder.toString();
    }

    @Override
    public String getQuery() {
        return this.query.toString();
    }

}
