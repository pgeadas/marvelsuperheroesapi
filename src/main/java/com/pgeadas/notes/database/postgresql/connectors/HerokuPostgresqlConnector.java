package com.pgeadas.notes.database.postgresql.connectors;

import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.database.strategy.ConnectorTypeStrategy;
import com.pgeadas.notes.database.strategy.ResultsStrategy;
import com.pgeadas.notes.exceptions.BadParameterException;
import com.pgeadas.notes.exceptions.DuplicatesNotAllowedException;
import com.pgeadas.notes.exceptions.UserDoesNotExistException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author geadas
 *
 * JDBC_DATABASE_URL
 *
 * Docs:
 * https://devcenter.heroku.com/articles/database-connection-pooling-with-java
 *
 * jdbc:postgresql://<host>:<port>/<dbname>?user=<username>&password=<password>
 */
public class HerokuPostgresqlConnector extends ConnectorTypeStrategy {

    static BasicDataSource connectionPool;
    static int INITIAL_POOL_SIZE = 5;

    private URI dbUri;
    private String dbUrl;
    private final PostgresqlQueryMaker sqlqm = new PostgresqlQueryMaker(this);

    public HerokuPostgresqlConnector() throws URISyntaxException {
        log.info("Initiating database connection.... ");
        initConnectionPool();
        log.info("Database connection established successfully. ");
    }

    /**
     * Separated this from init() cause of the exception it can throw trying to
     * get the URI, that is not defined in init()*
     */
    private void initConnectionPool() throws URISyntaxException {
        dbUri = new URI(System.getenv("DATABASE_URL"));
        dbUrl = "jdbc:postgresql://" + dbUri.getHost() + dbUri.getPath();
        init();
        dbUri = null;
        dbUrl = null;
    }

    @Override
    protected void init() {
        connectionPool = new BasicDataSource();
        if (dbUri.getUserInfo() != null) {
            connectionPool.setUsername(dbUri.getUserInfo().split(":")[0]);
            connectionPool.setPassword(dbUri.getUserInfo().split(":")[1]);
        }
        connectionPool.setDriverClassName("org.postgresql.Driver");
        connectionPool.setUrl(dbUrl);
        connectionPool.setInitialSize(INITIAL_POOL_SIZE);
    }

    @Override
    public void shutdown() {
        log.info("Closing database connection pool.... ");
        connectionPool = null;
        log.info("Closed connection to database.");
    }

    @Override
    public Object makeQuery(String query, ResultsStrategy rStrategy) throws SQLException {
        return sqlqm.makeQuery(query, rStrategy, connectionPool.getConnection());
    }

    @Override
    public long insertNote(Note note) throws SQLException, UserDoesNotExistException, NoSuchAlgorithmException, DuplicatesNotAllowedException {
        return sqlqm.insertNote(note);
    }

    @Override
    public long deleteNote(Note note) throws SQLException {
        return sqlqm.deleteNote(note);
    }

    @Override
    public long updateNote(Note note) throws SQLException {
        return sqlqm.updateNote(note);
    }

    @Override
    public List getNote(Note note) throws SQLException {
        return sqlqm.getNote(note);
    }

    @Override
    public Pair<Long, List<Note>> getNotes(String name, Pair limitAndOffset, Optional<HashMap<String, String>> queryParameters, Optional<Integer> creatorId) throws SQLException, BadParameterException {
        return sqlqm.getNotes(name, limitAndOffset, queryParameters, creatorId);
    }

}
