package com.pgeadas.notes.database.postgresql.algorithms;

import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.database.strategy.ResultsStrategy;
import static com.pgeadas.notes.database.strategy.ResultsStrategy.log;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author geadas
 */
public class UpdateNote implements ResultsStrategy {

    private final StringBuilder query;

    public UpdateNote(Note note) throws SQLException {
        query = new StringBuilder();
        buildUpdateNoteQuery(note.getDescription(), note.getHash(), note.getUsername());
    }

    @Override
    public Object getResultsFromQuery(ResultSet rs) throws SQLException {
        long count = -1;
        if (rs.next()) {
            count = rs.getInt(1);
        }
        log.debug("{} - Records updated: {}", this.getClass(), count);
        return count;
    }

    /**
     * Builds the query for updating notes. Returns 1 if the update was
     * successful, 0 otherwise.
     *
     * @param description
     * @param hash
     * @param username
     * @throws java.sql.SQLException
     */
    private void buildUpdateNoteQuery(String description, String hash, String username) throws SQLException {
        query.append("update notes n set description = '");
        query.append(description);
        query.append("', modification_date = (SELECT LOCALTIMESTAMP(0)) ");
        query.append(" from users u, user_notes un where un.user_id = u.id and un.note_id = n.id and u.name LIKE '");
        query.append(username);
        query.append("' and n.hash LIKE '");
        query.append(hash);
        query.append("' RETURNING 1;");
    }

    @Override
    public String getQuery() {
        return query.toString();
    }

}
