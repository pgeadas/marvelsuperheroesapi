package com.pgeadas.notes.database.postgresql.algorithms;

import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.constants.ServiceNotesTags;
import com.pgeadas.notes.database.strategy.ResultsStrategy;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author geadas
 */
public class GetNote implements ResultsStrategy {

    private final StringBuilder statement;
    private final StringBuilder stmt;

    public GetNote(Note note) throws SQLException {
        statement = new StringBuilder();
        stmt = new StringBuilder();
        stmt.append("select n.hash as ");
        stmt.append(ServiceNotesTags.hashTag);
        stmt.append(", n.creation_date as ");
        stmt.append(ServiceNotesTags.creationDateTag);
        stmt.append(", n.modification_date as ");
        stmt.append(ServiceNotesTags.modificationDateTag);
        stmt.append(", n.description as ");
        stmt.append(ServiceNotesTags.descriptionTag);
        stmt.append(", n.creator_name as ");
        stmt.append(ServiceNotesTags.creatorNameTag);
        stmt.append(", n.creator_id as ");
        stmt.append(ServiceNotesTags.creatorIdTag);
        stmt.append(" from notes n, user_notes un, users u where un.user_id = u.id and un.note_id = n.id");
        buildGetNoteQuery(note);
    }

    @Override
    public Object getResultsFromQuery(ResultSet rs) throws SQLException {
        List<Note> notes = new ArrayList<>();

        while (rs.next()) {
            notes.add(new Note(rs.getString(ServiceNotesTags.hashTag),
                    rs.getTimestamp(ServiceNotesTags.creationDateTag),
                    rs.getTimestamp(ServiceNotesTags.modificationDateTag),
                    rs.getString(ServiceNotesTags.descriptionTag),
                    rs.getString(ServiceNotesTags.creatorNameTag),
                    rs.getInt(ServiceNotesTags.creatorIdTag)
            ));
        }
        log.debug("{} - Records found: {}", this.getClass(), notes.size());
        return notes;
    }

    private void buildGetNoteQuery(Note note) throws SQLException {
        statement.append(stmt);
        statement.append(" and name ILIKE '");
        statement.append(note.getUsername());
        statement.append("' and n.hash LIKE '");
        statement.append(note.getHash());
        statement.append("'");
    }

    @Override
    public String getQuery() {
        return statement.toString();
    }

}
