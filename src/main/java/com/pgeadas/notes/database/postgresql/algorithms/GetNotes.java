package com.pgeadas.notes.database.postgresql.algorithms;

import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.constants.ServiceNotesTags;
import com.pgeadas.notes.database.strategy.ResultsStrategy;
import com.pgeadas.notes.exceptions.BadParameterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author geadas
 */
public class GetNotes implements ResultsStrategy {

    private final StringBuilder statement;
    private final StringBuilder stmt;

    public GetNotes() {
        statement = new StringBuilder();
        stmt = new StringBuilder();
        stmt.append("select n.hash as ");
        stmt.append(ServiceNotesTags.hashTag);
        stmt.append(", n.creation_date as ");
        stmt.append(ServiceNotesTags.creationDateTag);
        stmt.append(", n.modification_date as ");
        stmt.append(ServiceNotesTags.modificationDateTag);
        stmt.append(", n.description as ");
        stmt.append(ServiceNotesTags.descriptionTag);
        stmt.append(", n.creator_name as ");
        stmt.append(ServiceNotesTags.creatorNameTag);
        stmt.append(", n.creator_id as ");
        stmt.append(ServiceNotesTags.creatorIdTag);
        stmt.append(" from notes n, user_notes un, users u");
    }

    @Override
    public Object getResultsFromQuery(ResultSet rs) throws SQLException {
        List<Note> notes = new ArrayList<>();
        while (rs.next()) {
            notes.add(new Note(rs.getString(ServiceNotesTags.hashTag),
                    rs.getTimestamp(ServiceNotesTags.creationDateTag),
                    rs.getTimestamp(ServiceNotesTags.modificationDateTag),
                    rs.getString(ServiceNotesTags.descriptionTag),
                    rs.getString(ServiceNotesTags.creatorNameTag),
                    rs.getInt(ServiceNotesTags.creatorIdTag)
            ));
        }
        log.debug("{} - Records found: {}", this.getClass(), notes.size());

        return notes;
    }

    /**
     * Builds the whereClause used by "getCount", to count all the matches with
     * the respective filtering and at the same time builds the final query to
     * get the Notes.
     *
     *
     * @param name
     * @param limitAndOffset
     * @param queryParameters
     * @param creatorId
     * @return
     * @throws com.pgeadas.notes.exceptions.BadParameterException
     */
    public String getWhereClause(String name, Pair limitAndOffset, Optional<HashMap<String, String>> queryParameters, Optional<Integer> creatorId) throws BadParameterException {
        statement.append(stmt);
        StringBuilder whereClause = new StringBuilder();
        whereClause.append(" where un.user_id = u.id and un.note_id = n.id");
        //builds the where clause used to count results (basically the same as statement but without limit and offset)
        if (queryParameters.isPresent()) {
            whereClause = buildWhereClauseFromURI(queryParameters.get(), whereClause);
        }
        //in this case we want to make a query by creator_id (which was not provided by the user) so we pass it as optional
        if (creatorId.isPresent()) {
            whereClause.append(" and n.creator_id = ");
            whereClause.append(creatorId.get());
        }
        addUsernameFilter(name, whereClause);
        //appends it to the final query
        statement.append(whereClause);
        //appends additional information (limit and offset) to the final query
        statement.append(" limit ");
        statement.append(limitAndOffset.getLeft());
        statement.append(" offset ");
        statement.append(limitAndOffset.getRight());

        return whereClause.toString();
    }

    private StringBuilder buildWhereClauseFromURI(HashMap<String, String> queryParameters, StringBuilder whereClause) throws BadParameterException {
        checkCreationDate(queryParameters, whereClause, ServiceNotesTags.creationDateTag);
        checkModificationDate(queryParameters, whereClause, ServiceNotesTags.modificationDateTag);
        checkCreatorId(queryParameters, whereClause, ServiceNotesTags.creatorIdTag);
        checkCreatorNameContains(queryParameters, whereClause, ServiceNotesTags.creatorNameContainsTag);
        checkCreatorNameEquals(queryParameters, whereClause, ServiceNotesTags.creatorNameEqualsTag);
        checkNoteDescriptionContainsText(queryParameters, whereClause, ServiceNotesTags.descriptionContainsTag);
        checkNoteDescriptionEqualsText(queryParameters, whereClause, ServiceNotesTags.descriptionEqualsTag);
        return whereClause;
    }

    private StringBuilder checkCreationDate(HashMap<String, String> queryParameters, StringBuilder whereClause, String key) {
        if (checkNullUriParameter(queryParameters, key)) {
            LocalDate date = LocalDate.parse((String) queryParameters.get(key));
            whereClause.append(" and creation_date >= (TIMESTAMP '");
            whereClause.append(date);
            whereClause.append("') and creation_date < (TIMESTAMP '");
            whereClause.append(date.plusDays(1).toString());
            whereClause.append("') ");
        }
        return whereClause;
    }

    private StringBuilder checkModificationDate(HashMap<String, String> queryParameters, StringBuilder whereClause, String key) {
        if (checkNullUriParameter(queryParameters, key)) {
            LocalDate date = LocalDate.parse((String) queryParameters.get(key));
            whereClause.append(" and modification_date >= (TIMESTAMP '");
            whereClause.append(date);
            whereClause.append("') and modification_date < (TIMESTAMP '");
            whereClause.append(date.plusDays(1).toString());
            whereClause.append("') ");
        }
        return whereClause;
    }

    private StringBuilder checkCreatorId(HashMap<String, String> queryParameters, StringBuilder whereClause, String key) throws BadParameterException {
        if (checkNullUriParameter(queryParameters, key)) {
            try {
                int id = Integer.parseInt(queryParameters.get(key));
                whereClause.append(" and creator_id = ");
                whereClause.append(id);
            } catch (Exception ex) {
                //if id is not a number, just ignore this parameter or we could throw an exception to warn the user
                throw new BadParameterException("Invalid creator ID.");
            }
        }
        return whereClause;
    }

    private StringBuilder checkCreatorNameContains(HashMap<String, String> queryParameters, StringBuilder whereClause, String key) {
        if (checkNullUriParameter(queryParameters, key)) {
            String creatorName = (String) queryParameters.get(key);
            whereClause.append(" and creator_name ILIKE '%");
            whereClause.append(creatorName);
            whereClause.append("%'");
        }
        return whereClause;
    }
        private StringBuilder checkCreatorNameEquals(HashMap<String, String> queryParameters, StringBuilder whereClause, String key) {
        if (checkNullUriParameter(queryParameters, key)) {
            String creatorName = (String) queryParameters.get(key);
            whereClause.append(" and creator_name ILIKE '");
            whereClause.append(creatorName);
            whereClause.append("'");
        }
        return whereClause;
    }

    private StringBuilder checkNoteDescriptionContainsText(HashMap<String, String> queryParameters, StringBuilder whereClause, String key) {
        if (checkNullUriParameter(queryParameters, key)) {
            String text = (String) queryParameters.get(key);
            whereClause.append(" and description ILIKE '%");
            whereClause.append(text);
            whereClause.append("%'");
        }
        return whereClause;
    }

    private StringBuilder checkNoteDescriptionEqualsText(HashMap<String, String> queryParameters, StringBuilder whereClause, String key) {
        if (checkNullUriParameter(queryParameters, key)) {
            String text = (String) queryParameters.get(key);
            whereClause.append(" and description ILIKE '");
            whereClause.append(text);
            whereClause.append("'");
        }
        return whereClause;
    }

    private StringBuilder addUsernameFilter(String name, StringBuilder whereClause) {
        whereClause.append(" and name ILIKE '");
        whereClause.append(name);
        whereClause.append("'");
        return whereClause;
    }

    @Override
    public String getQuery() {
        return statement.toString();
    }

}
