package com.pgeadas.notes.database.strategy;

import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.exceptions.BadParameterException;
import com.pgeadas.notes.exceptions.DuplicatesNotAllowedException;
import com.pgeadas.notes.exceptions.UserDoesNotExistException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author geadas TODO: wrap the concrete connector exceptions with a Custom
 * databaseException, so the method signature is the same to all strategy
 * "algorithms" * TODO: sanitize passed parameters and use prepared
 * statements...
 */
public class DatabaseContext {

    //initialize a database context with the desired DB connector
    private final ConnectorTypeStrategy connector;

    public DatabaseContext(ConnectorTypeStrategy strategy) {
        this.connector = strategy;
    }

    public Long insertNote(Note note) throws SQLException, NoSuchAlgorithmException, DuplicatesNotAllowedException, UserDoesNotExistException {
        return connector.insertNote(note);
    }

    public List getNote(Note note) throws SQLException {
        return connector.getNote(note);
    }

    public Pair<Long, List<Note>> getNoteS(String name, Pair limitAndOffset, Optional<HashMap<String, String>> queryParameters, Optional<Integer> creatorId) throws SQLException, BadParameterException {
        return connector.getNotes(name, limitAndOffset, queryParameters, creatorId);
    }

    public Long deleteNote(Note note) throws SQLException {
        return connector.deleteNote(note);
    }

    public Long updateNote(Note note) throws SQLException {
        return connector.updateNote(note);
    }

    public ConnectorTypeStrategy getActualConnector() {
        return connector;
    }

}
