package com.pgeadas.notes.database.strategy;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author geadas
 */
public interface ResultsStrategy {

    public static final Logger log = LoggerFactory.getLogger(ResultsStrategy.class);

    public Object getResultsFromQuery(ResultSet rs) throws SQLException;

    public String getQuery();

    default boolean checkNullUriParameter(HashMap<String, String> queryParameters, String param) {
        return queryParameters.get(param) != null;
    }

}
