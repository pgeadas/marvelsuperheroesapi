package com.pgeadas.notes.database.strategy;

import com.pgeadas.notes.beans.beans.Note;
import com.pgeadas.notes.exceptions.BadParameterException;
import com.pgeadas.notes.exceptions.DuplicatesNotAllowedException;
import com.pgeadas.notes.exceptions.UserDoesNotExistException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author geadas //TODO: wrap the SQLexception with custom DatabaseException
 * when more connectors are added
 */
public abstract class ConnectorTypeStrategy {

    protected final static Logger log = LoggerFactory.getLogger(ConnectorTypeStrategy.class);

    protected abstract void init();
    
    public abstract void shutdown();
    
    public abstract Object makeQuery(String query, ResultsStrategy rStrategy) throws SQLException;

    public abstract long insertNote(Note note) throws SQLException, NoSuchAlgorithmException, DuplicatesNotAllowedException, UserDoesNotExistException;

    public abstract long updateNote(Note note) throws SQLException;

    public abstract long deleteNote(Note note) throws SQLException;

    public abstract List getNote(Note note) throws SQLException;

    public abstract Pair<Long, List<Note>> getNotes(String name, Pair limitAndOffset, Optional<HashMap<String, String>> queryParameters, Optional<Integer> creatorId) throws SQLException, BadParameterException;
}
