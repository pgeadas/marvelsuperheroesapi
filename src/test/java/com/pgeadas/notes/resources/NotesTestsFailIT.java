package com.pgeadas.notes.resources;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.http.ContentType.JSON;
import java.io.IOException;
import org.apache.http.HttpStatus;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.hasKey;
import org.junit.Test;

/**
 *
 * @author geadas
 */
public class NotesTestsFailIT extends IntegrationTest {

    public NotesTestsFailIT() {
    }

    @Test
    public void schemaErrors1() throws IOException {
        String expected = "Failed schema validation: [#/description: expected type: String, found: Boolean, #: extraneous key [creatorName] is not permitted]";
        given()
                .contentType(JSON)
                .body(invalidPostSchema)
                .when()
                .post("/v1/Pedro/notes/add/")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .assertThat().body("'response info'.'status message'", containsString(expected))
                .assertThat().body("$", not(hasKey("'response info'.'result count'")));
    }

    @Test
    public void schemaErrors2() throws IOException {
        String expected = "Failed schema validation: [#: required key [description] not found, #: required key [creatorId] not found]";
        given()
                .contentType(JSON)
                .body(missingRequiredKey)
                .when()
                .post("/v1/Pedro/notes/add/")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .assertThat().body("'response info'.'status message'", containsString(expected))
                .assertThat().body("$", not(hasKey("'response info'.'result count'")));
    }

    @Test
    public void testJSONMalformed() throws IOException {
        given()
                .contentType(JSON)
                .body(malformedJSON)
                .when()
                .post("/v1/Pedro/notes/add/")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST)
                .assertThat().body("'response info'.'status message'", containsString("Invalid JSON"))
                .assertThat().body("$", not(hasKey("'response info'.'result count'")));
    }

    @Test
    public void testAddNonExistingUser() throws IOException {
        String expected = "Failed to insert Note: User does not exist";
        given()
                .contentType(JSON)
                .body(validPostRequest)
                .when()
                .post("/v1/Pedroooo/notes/add/")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST)
                .assertThat().body("'response info'.'status message'", containsString(expected));
    }

}
