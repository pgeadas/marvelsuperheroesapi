package com.pgeadas.notes.resources;

import static com.jayway.restassured.RestAssured.given;
import static com.pgeadas.notes.resources.IntegrationTest.testAddSuccess;
import static com.pgeadas.notes.resources.IntegrationTest.testDelSuccess;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpStatus;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import org.junit.AfterClass;
import org.junit.Test;

/**
 *
 * @author geadas
 */
public final class NotesTestsSuccess extends IntegrationTest {

    public NotesTestsSuccess() {
        try {
            testAddNotes();
        } catch (IOException ex) {
            Logger.getLogger(NotesTestsSuccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //add the initial notes to the database
    public static void testAddNotes() throws IOException {
        testAddSuccess(getAddJson("My first note", "6606"));
        testAddSuccess(getAddJson("My second note", "6606"));
        testAddSuccess(getAddJson("My third note", "6606"));
        testAddSuccess(getAddJson("My first note", "12844"));
        testAddSuccess(getAddJson("My second note", "12844"));
        testAddSuccess(getAddJson("My third note", "12844"));
        testAddSuccess(getAddJson("My seventh note", "2289"));
        testAddSuccess(getAddJson("My eighth note", "2289"));
        testAddSuccess(getAddJson("My ninth note", "2289"));
        testAddSuccess(getAddJson("My tenth note", "2289"));
    }

    @AfterClass
    public static void testdelNotes() throws IOException {
        testDelSuccess(getDelJson("cd7d1194c9c15faf438d6cfda783a1b3"));
        testDelSuccess(getDelJson("2836923b0d784dc047759378668e6062"));
        testDelSuccess(getDelJson("ea9f91b036b3d4c60491debefbec7970"));
        testDelSuccess(getDelJson("871f32665bcb0bc4d81b56c3f2aa705b"));
        testDelSuccess(getDelJson("d7ed38f3cc84422e7666ad2caa5b710f"));
        testDelSuccess(getDelJson("bdabdaa5e4423e0943390cb4b0c4bfe0"));
        testDelSuccess(getDelJson("89cd4af4d53f1a8c3ca9af7536b6b233"));
        testDelSuccess(getDelJson("eab3f52c412667996fa921cd2ad36ee5"));
        testDelSuccess(getDelJson("1ab86bc848bc090b51dc489630b9fc43"));
        testDelSuccess(getDelJson("86478bfb1e3fb0df0b800c3f616a4d03"));
    }

    @Test
    public void tests() {
        testListNotesGetAll("/v1/Pedro/notes/");
        testListNotesByCreatorIdExisting("/v1/Pedro/notes?creatorId=6606");
        testListNotesByCreatorIdNotExisting("/v1/Pedro/notes?creatorId=123123411");
        testListNotesContainsString("/v1/Pedro/notes?descriptionContains=first");
        testListNotesEqualsString("/v1/Pedro/notes?descriptionEquals=My first note");
        testListNotesNotEqualsString("/v1/Pedro/notes?descriptionEquals=not");

        testListNotesCustomLimitAndOffset("/v1/Pedro/notes?limit=%d&offset=%d", 2, 2);
        testListNotesOffsetOutOfRange("/v1/Pedro/notes?offset=%d", 20);

        //creatorName
        testListNotesResultsCounts("/v1/Pedro/notes?creatorNameContains=A", 5, 10);
        testListNotesResultsCounts("/v1/Pedro/notes?creatorNameEquals=Aco", 3, 3);
        testListNotesResultsCounts("/v1/Pedro/notes?creatorNameEquals=not", 0, 0);
        //dates 
        testListNotesResultsCounts("/v1/Pedro/notes?creationDate=2017-08-10", 0, 0);
        testListNotesResultsCounts("/v1/Pedro/notes?modificationDate=2017-09-06", 0, 0);
        //this test is commented because it depends on the "today" date, that is the date of insertion of the Notes in the database...
        //testListNotesCreationDateMatch("/v1/Pedro/notes?creationDate=", "2017-07-09");
    }

    public void testListNotesResultsCounts(String uri, int count, int total) {
        given()
                .when()
                .get(uri)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(responseInfoCount, equalTo(count))
                .assertThat().body(responseInfoTotalCount, equalTo(total));
    }

    public void testListNotesOffsetOutOfRange(String uri, int offset) {
        String first = "http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes?limit=5&offset=0";
        String last = "http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes?limit=5&offset=5";
        String previous = String.format("http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes?limit=5&offset=15", offset - 5);
        String next = "";
        given()
                .when()
                .get(String.format(uri, offset))
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(responseInfoCount, equalTo(0))
                .assertThat().body(responseInfoTotalCount, equalTo(10))
                .assertThat().body(responseInfoNext, equalTo(next))
                .assertThat().body(responseInfoPrevious, equalTo(previous))
                .assertThat().body(responseInfoLast, equalTo(last))
                .assertThat().body(responseInfoFirst, equalTo(first));
    }

    public void testListNotesCustomLimitAndOffset(String uri, int limit, int offset) {
        given()
                .when()
                .get(String.format(uri, limit, offset))
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(responseInfoCount, equalTo(2))
                .assertThat().body(responseInfoTotalCount, equalTo(10))
                .assertThat().body(responseInfoNext, equalTo("http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes?limit=2&offset=4"))
                .assertThat().body(responseInfoPrevious, equalTo("http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes?limit=2&offset=0"));
    }

    public void testListNotesCreationDateMatch(String uri, String date) {
        given()
                .when()
                .get(uri.concat(date))
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(responseInfoCount, equalTo(5))
                .assertThat().body(responseInfoTotalCount, equalTo(10))
                .assertThat().body(responseInfoNext, equalTo("http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes?limit=5&offset=5&creationDate=".concat(date)))
                .assertThat().body(responseInfoPrevious, equalTo(""));

    }

    public void testListNotesModificationDateMatch(String uri) {
        given()
                .when()
                .get(uri)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(responseInfoCount, equalTo(5))
                .assertThat().body(responseInfoTotalCount, equalTo(10))
                .assertThat().body(responseInfoNext, equalTo("http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes?limit=5&offset=5&modificationDate=2017-07-08"))
                .assertThat().body(responseInfoPrevious, equalTo(""));

    }

    public void testListNotesGetAll(String uri) {
        given()
                .when()
                .get(uri)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(responseInfoCount, equalTo(5))
                .assertThat().body(responseInfoTotalCount, equalTo(10))
                .assertThat().body(responseInfoNext, equalTo("http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/notes?limit=5&offset=5"))
                .assertThat().body(responseInfoPrevious, equalTo(""));
    }

    public void testListNotesByCreatorIdExisting(String uri) {
        given()
                .when()
                .get(uri)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(responseInfoCount, equalTo(3))
                .assertThat().body(responseInfoTotalCount, equalTo(3))
                .assertThat().body(responseInfoNext, equalTo(""))
                .assertThat().body(responseInfoPrevious, equalTo(""))
                .assertThat().body(responseDataCreatorName, equalTo("A.R.K."))
                .assertThat().body(responseDataCreatorId, equalTo(6606));
    }

    public void testListNotesByCreatorIdNotExisting(String uri) {
        given()
                .when()
                .get(uri)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(responseInfoCount, equalTo(0))
                .assertThat().body(responseInfoTotalCount, equalTo(0));
    }

    public void testListNotesContainsString(String uri) {
        given()
                .when()
                .get(uri)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(responseInfoCount, equalTo(2))
                .assertThat().body(responseInfoTotalCount, equalTo(2))
                .assertThat().body(responseInfoNext, equalTo(""))
                .assertThat().body(responseInfoPrevious, equalTo(""))
                .assertThat().body(responseDataCreatorName, hasItems("A.R.K.", "Aco"))
                .assertThat().body(responseDataCreatorId, hasItems(6606, 12844))
                .assertThat().body(responseDataZeroDescription, containsString("first"))
                .assertThat().body(responseDataOneDescription, containsString("first"));
    }

    public void testListNotesEqualsString(String uri) {
        given()
                .when()
                .get(uri)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(responseInfoCount, equalTo(2))
                .assertThat().body(responseInfoTotalCount, equalTo(2))
                .assertThat().body(responseInfoNext, equalTo(""))
                .assertThat().body(responseInfoPrevious, equalTo(""))
                .assertThat().body(responseDataCreatorName, hasItems("A.R.K.", "Aco"))
                .assertThat().body(responseDataCreatorId, hasItems(6606, 12844))
                .assertThat().body(responseDataZeroDescription, equalTo("My first note"))
                .assertThat().body(responseDataOneDescription, equalTo("My first note"));
    }

    public void testListNotesNotEqualsString(String uri) {
        given()
                .when()
                .get(uri)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(responseInfoCount, equalTo(0))
                .assertThat().body(responseInfoTotalCount, equalTo(0));
    }
}
