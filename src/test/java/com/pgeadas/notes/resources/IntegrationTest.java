package com.pgeadas.notes.resources;

import com.jayway.restassured.RestAssured;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.http.ContentType.JSON;
import com.pgeadas.notes.constants.Functions;
import com.pgeadas.notes.constants.ServiceCommons;
import com.pgeadas.notes.constants.ServiceNotesTags;
import java.io.IOException;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;

/**
 *
 * @author geadas
 */
public class IntegrationTest {

    //paths to search in the Response Json
    protected static String validDeleteRequest, validPostRequest, invalidPostSchema, missingRequiredKey, validPutRequest, invalidPutRequest, malformedJSON;
    protected String responseInfoCount = "'".concat(ServiceCommons.responseInfoTag).concat("'.").concat(ServiceCommons.countTag);
    protected String responseInfoTotalCount = "'".concat(ServiceCommons.responseInfoTag).concat("'.").concat(ServiceCommons.totalCountTag);
    protected String responseInfoNext = "'".concat(ServiceCommons.responseInfoTag).concat("'.").concat(ServiceCommons.nextTag);
    protected String responseInfoPrevious = "'".concat(ServiceCommons.responseInfoTag).concat("'.").concat(ServiceCommons.previousTag);
    protected String responseInfoLast = "'".concat(ServiceCommons.responseInfoTag).concat("'.").concat(ServiceCommons.lastTag);
    protected String responseInfoFirst = "'".concat(ServiceCommons.responseInfoTag).concat("'.").concat(ServiceCommons.firstTag);
    protected String responseDataCreatorId = "'".concat(ServiceCommons.responseDataTag).concat("'.").concat(ServiceNotesTags.creatorIdTag);
    protected String responseDataCreatorIdZero = "'".concat(ServiceCommons.responseDataTag).concat("'[0].").concat(ServiceNotesTags.creatorIdTag);
    protected String responseDataCreatorName = "'".concat(ServiceCommons.responseDataTag).concat("'.").concat(ServiceNotesTags.creatorNameTag);
    protected String responseDataCreatorNameZero = "'".concat(ServiceCommons.responseDataTag).concat("'[0].").concat(ServiceNotesTags.creatorNameTag);
    protected String responseDataZeroDescription = "'".concat(ServiceCommons.responseDataTag).concat("'[0].").concat(ServiceNotesTags.descriptionTag);
    protected String responseDataOneDescription = "'".concat(ServiceCommons.responseDataTag).concat("'[1].").concat(ServiceNotesTags.descriptionTag);
    //

    public IntegrationTest() {
    }

    private static void setUpServer() {
        System.out.println("Setting up server before executing integration tests...");

        String port = System.getProperty("server.port");
        if (port == null) {
            RestAssured.port = 8080;
        } else {
            RestAssured.port = Integer.valueOf(port);
        }

        String basePath = System.getProperty("server.base");
        if (basePath == null) {
            basePath = "/MarvelSuperHeroesAPI/";
        }
        RestAssured.basePath = basePath;

        String baseHost = System.getProperty("server.host");
        if (baseHost == null) {
            baseHost = "http://localhost";
        }
        RestAssured.baseURI = baseHost;

    }

    protected static String getAddJson(String description, String creatorId) {
        return String.format("{\"description\": \"%s\",\"creatorId\" : %s}", description, creatorId);
    }

    protected static String getDelJson(String description) {
        return String.format("{\"hash\": \"%s\"}", description);
    }

    public static void testAddSuccess(String json) throws IOException {
        given()
                .contentType(JSON)
                .body(json)
                .when()
                .post("/v1/Pedro/notes/add")
                .then().statusCode(HttpStatus.SC_OK);
    }

    public static void testDelSuccess(String json) throws IOException {
        given()
                .contentType(JSON)
                .body(json)
                .when()
                .delete("/v1/Pedro/notes/delete")
                .then().statusCode(HttpStatus.SC_OK);
    }

    //to pass diferent parameters, using command line: mvn test -Dserver.port=9000 -Dserver.host=http://example.com
    @BeforeClass
    public static void setUpClass() throws IOException {
        readRequests();
        setUpServer();
    }

    private static void readRequests() throws IOException {
        malformedJSON = "{description\":\"\"}";
        validDeleteRequest = Functions.getResourceAsStream("DEL_IT/validRequest.json", ServiceCommons.ENCODING);
        validPostRequest = Functions.getResourceAsStream("POST_IT/validRequest.json", ServiceCommons.ENCODING);
        missingRequiredKey = Functions.getResourceAsStream("POST_IT/missingRequiredKey.json", ServiceCommons.ENCODING);
        invalidPostSchema = Functions.getResourceAsStream("POST_IT/invalidSchema.json", ServiceCommons.ENCODING);
        validPutRequest = Functions.getResourceAsStream("PUT_IT/validRequest.json", ServiceCommons.ENCODING);
        invalidPutRequest = Functions.getResourceAsStream("PUT_IT/invalidRequest.json", ServiceCommons.ENCODING);
    }

}
