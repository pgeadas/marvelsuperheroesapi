package com.pgeadas.notes.resources;

import static com.jayway.restassured.RestAssured.given;
import com.pgeadas.notes.constants.ServiceCommons;
import com.pgeadas.notes.constants.ServiceCreatorsTags;
import org.apache.http.HttpStatus;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import org.junit.Test;

/**
 *
 * @author geadas In the testNotes, we added custom notes to creators with id =
 * 6606, 12844 and 2289 Those were chosen because they are returned in the first
 * page of a call to the marvel API, which makes it easier to debug the app
 */
public class CreatorsListIT extends IntegrationTest {

    String pathToResults = "'response info'.";
    String creatorsFullname = pathToResults.concat(ServiceCreatorsTags.creatorsResultsTag.concat(".").concat(ServiceCreatorsTags.fullNameTag));

    public CreatorsListIT() {

    }

    //customFiltering: http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/creators/list?fullNameEquals=A.R.K.&fullNameContains=A.R.
    @Test
    public void testGetList() {
        testCreatorsListGetAll("/v1/Pedro/creators/list");
        testCreatorsListGetAllCustomLimitAndOffset("/v1/Pedro/creators/list?limit=%d&offset=%d", 5, 5);
        testCreatorsListGetAllCustomLimitAndOffset("/v1/Pedro/creators/list?limit=%d&offset=%d", 10, 10);
    }

    public void testCreatorsListGetAllAndCheckNotes(String uri, int creatorId, int noteCount) {
        given()
                .when()
                .get(uri)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(creatorsFullname, hasItems("Aco", "A.R.K.", "Alexandrov"));
    }

    /**
     * Checks the results counts and that our 3 creators with notes are in the
     * first page returned
     *
     *
     * @param uri
     */
    public void testCreatorsListGetAll(String uri) {

        given()
                .when()
                .get(uri)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(pathToResults.concat(ServiceCommons.countTag), equalTo(20))
                .assertThat().body(pathToResults.concat(ServiceCommons.totalCountTag), equalTo(6115))
                .assertThat().body(pathToResults.concat(ServiceCommons.limitTag), equalTo(20))
                .assertThat().body(pathToResults.concat(ServiceCommons.offsetTag), equalTo(0))
                .assertThat().body(creatorsFullname, hasItems("Aco", "A.R.K.", "Alexandrov"));
        
    }

    public void testCreatorsListGetAllCustomLimitAndOffset(String uri, int limit, int offset) {
        given()
                .when()
                .get(String.format(uri, limit, offset))
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(pathToResults.concat(ServiceCommons.totalCountTag), equalTo(6115))
                .assertThat().body(pathToResults.concat(ServiceCommons.limitTag), equalTo(limit))
                .assertThat().body(pathToResults.concat(ServiceCommons.offsetTag), equalTo(offset));
    }

}
