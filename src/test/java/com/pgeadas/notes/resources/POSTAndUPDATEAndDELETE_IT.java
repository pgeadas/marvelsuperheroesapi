package com.pgeadas.notes.resources;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.http.ContentType.JSON;
import java.io.IOException;
import org.apache.http.HttpStatus;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.Test;

/**
 *
 * @author geadas
 */
public class POSTAndUPDATEAndDELETE_IT extends IntegrationTest {

    private static void deleteNote() {
        given()
                .contentType(JSON)
                .body(validDeleteRequest)
                .when()
                .delete("/v1/Pedro/notes/delete/");
    }

    @Test
    public void testCreateAndUpdateAndDelete() throws IOException {
        deleteNote();
        //adds a note
        testAddSuccess();
        //tries to add duplicate of the previous note
        testAddDuplicate();
        //tries to modify the note
        testUpdateSuccess();
        //tries to update wrong hash note
        testUpdateFail(invalidPutRequest);
        //since PUT is idempotent, lets try to update the valid note again, result will be always the same
        testUpdateSuccess();
        //Delete the note
        testDeleteSuccess();
        //tries to delete the same note again
        testDeleteFail();
        //tries to update the note that was deleted
        testUpdateFail(validPutRequest);
    }

    public void testAddDuplicate() throws IOException {
        String expected = "Failed to insert Note: Duplicates not allowed";
        given()
                .contentType(JSON)
                .body(validPostRequest)
                .when()
                .post("/v1/Pedro/notes/add/")
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body("'response info'.'status message'", containsString(expected));
    }

    public void testAddSuccess() throws IOException {
        String expected = "Note inserted.";
        given()
                .contentType(JSON)
                .body(validPostRequest)
                .when()
                .post("/v1/Pedro/notes/add/")
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body("'response info'.'status message'", containsString(expected));
    }

    public void testDeleteSuccess() throws IOException {
        String expected = "Note deleted";
        given()
                .contentType(JSON)
                .body(validDeleteRequest)
                .when()
                .delete("/v1/Pedro/notes/delete/")
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body("'response info'.'status message'", containsString(expected));
    }

    public void testDeleteFail() throws IOException {
        String expected = "Note not found";
        given()
                .contentType(JSON)
                .body(validDeleteRequest)
                .when()
                .delete("/v1/Pedro/notes/delete/")
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body("'response info'.'status message'", containsString(expected));
    }

    public void testUpdateSuccess() throws IOException {
        String expected = "Note updated.";
        given()
                .contentType(JSON)
                .body(validPutRequest)
                .when()
                .put("/v1/Pedro/notes/update/")
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body("'response info'.'status message'", containsString(expected));
    }

    public void testUpdateFail(String json) throws IOException {
        String expected = "Note not found.";
        given()
                .contentType(JSON)
                .body(json)
                .when()
                .put("/v1/Pedro/notes/update/")
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body("'response info'.'status message'", containsString(expected));
    }
}
