package com.pgeadas.notes.resources;

import static com.jayway.restassured.RestAssured.given;
import com.pgeadas.notes.constants.ServiceCreatorsTags;
import com.pgeadas.notes.helpers.JSONObjectBuilder;
import java.util.LinkedList;
import org.apache.http.HttpStatus;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import org.json.JSONObject;
import org.junit.Test;

/**
 *
 * @author geadas
 *
 * /***
 */
public final class ComparatorIT extends IntegrationTest {

    static String compareURI = "http://localhost:8080/MarvelSuperHeroesAPI/v1/Pedro/creators/compare";

    static JSONObject creator1, creator2;

    String pathToResults = "'response info'.";
    String creatorsArray = pathToResults.concat(ServiceCreatorsTags.creatorsResultsTag.concat("."));
    String pathToShared = pathToResults.concat(ServiceCreatorsTags.commonResourceCountTag.concat("."));

    public ComparatorIT() {
        creator1 = JSONObjectBuilder.
                buildCreatorForSummary(5638, "Raul", "", "Fernandez", "2013-01-28T17:16:22-0500", 42, 0, 41, 15, new JSONObject(new LinkedList<>()));
        creator2 = JSONObjectBuilder.
                buildCreatorForSummary(410, "Rus", "", "Wooton", "2010-11-24T13:52:31-0500", 365, 9, 354, 123, new JSONObject(new LinkedList<>()));

    }

    @Test
    public void testCompare() {
        testCompareCreators(creator1, creator2);
        testCreatorNotFound(creator1);
    }

    public void testCompareCreators(JSONObject e1, JSONObject e2) {

        given()
                .when()
                .get(compareURI + "/" + e1.get("id") + "/" + e2.get("id"))
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat()
                .body(creatorsArray.concat(ServiceCreatorsTags.firstNameTag), hasItems("Raul", "Rus"))
                .body(creatorsArray.concat(ServiceCreatorsTags.middleNameTag), hasItems("", ""))
                .body(creatorsArray.concat(ServiceCreatorsTags.lastNameTag), hasItems("Fernandez", "Wooton"))
                .body(creatorsArray.concat(ServiceCreatorsTags.modifiedTag), hasItems("2010-11-24T13:52:31-0500", "2013-01-28T17:16:22-0500"))
                .body(pathToShared.concat(ServiceCreatorsTags.commonEventsCountTag), equalTo(9))
                .body(pathToShared.concat(ServiceCreatorsTags.commonStoriesCountTag), equalTo(381))
                .body(pathToShared.concat(ServiceCreatorsTags.commonSeriesCountTag), equalTo(136))
                .body(pathToShared.concat(ServiceCreatorsTags.commonComicsCountTag), equalTo(400))
                .body(creatorsArray.concat(ServiceCreatorsTags.eventsTag), hasItems(0, 9))
                .body(creatorsArray.concat(ServiceCreatorsTags.storiesTag), hasItems(41, 354))
                .body(creatorsArray.concat(ServiceCreatorsTags.seriesTag), hasItems(123, 15))
                .body(creatorsArray.concat(ServiceCreatorsTags.comicsTag), hasItems(42, 365));

    }

    public void testCreatorNotFound(JSONObject e1) {
        given()
                .when()
                .get(compareURI + "/" + e1.get("id") + "/" + 41034)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat()
                .body(pathToResults.concat("'status message'.status"), equalTo("We couldn't find that creator"));
    }

}
